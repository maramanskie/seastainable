## Canvas / Discord group number: 
10AM Group 6

## Names of the team members:
* Caleb Park
* Carlos Vela
* Geethika Hemukmar
* Mara Manskie
* Derrick Liu

## Name of Project: Seastainable

## Gitlab URL: https://gitlab.com/maramanskie/seastainable

## URL’s of RESTful API’s:
* https://developer.edamam.com/edamam-recipe-api
* https://www.programmableweb.com/api/food
* https://developer.kroger.com/reference/
* https://www.fishwatch.gov/resources
* https://spoonacular.com/food-api

## Models
* Recipes
* Ingredient
* Fish

## Instances: 
* Recipes - 100+ instances
* Ingredients - 100+ instances
* Fish - 100+ instance

## Fish Model
* Image
* Filter/Sort
    * Population status
    * Nutritional value
    * Sustainability
        * Availability
        * Predators
    * Identifying features
* Search
    * Species/Name
        * Common name
        * Scientific name
    * Location
        * Habitat
        * Region(s)
        * Body of water

## Recipe
* Image
* Instructions
* Filter/Sort
    * Rating
    * Cost
    * Calories
    * Nutrition Info
    * Diet
    * Number of servings
    * Number of ingredients
* Search
    * Name
    * Cook/Prep time
    * Ingredients
    * Type of fish
    * Type of cuisine

## Ingredient
* Filter/Sort
    * Price
    * Macros (Protien, Calories, Fat)
    * Type (Vegetable, Fruit, Meat)
    * Name
    * Availability
* Search 
    * Nutrition
    * Locations available
    * Description
    * Picture
    * Recipes with Ingredient

## Media:
* Fish
    * Picture of fish Image
    * Map of primary location/habitat area
* Ingredient
    * Picture
    * Nutrition
* Recipe
    * Picture of recipe / end result 
    * Map Location (area of origin of corresponding fish)
    * Instructions on how to make the recipe

## Data Model Connections

* Fish → Ingredient : What is the nutritional info on the fish?
* Fish → Recipe : What types of recipes use a type of fish

* Ingredient → Fish: What are some facts about the fish and its environment
* Ingredient → Recipe: What recipes include this ingredient

* Recipe → Fish : What kinds of fish the recipe uses
* Recipe → Ingredient: What are the details about specific ingredients in a recipe

## Questions
* What types of fish should I avoid to help the environment?
* How can I eat sustainable seafood?
* What are the nutritional values of a variety of fish species?

