FRONTEND_DIR := frontend
BACKEND_DIR := backend
SHELL := /bin/bash

build-frontend: ${FRONTEND_DIR}
	npm run build --prefix frontend

start-frontend: ${FRONTEND_DIR}
	npm start --prefix frontend

start-backend: ${BACKEND_DIR}
	python3 backend/app.py

build-frontend-docker: ${FRONTEND_DIR}
	docker build -t frontend-docker -f ${FRONTEND_DIR}/Dockerfile ${FRONTEND_DIR}

run-frontend-docker: ${FRONTEND_DIR}
	docker run -it -v "$(shell pwd)/frontend:/frontend" -w /frontend frontend-docker

build-backend-docker-dev: ${BACKEND_DIR}
	docker build -t flask-docker-dev -f ${BACKEND_DIR}/dev.Dockerfile ${BACKEND_DIR}

run-backend-docker-dev: ${BACKEND_DIR}
	docker run -it -v "$(shell pwd)/backend:/usr/src/backend" -w /usr/src/backend -p 5000:5000 flask-docker-dev

build-backend-docker-prod: ${BACKEND_DIR}
	docker build -t flask-docker -f ${BACKEND_DIR}/Dockerfile ${BACKEND_DIR}

run-backend-docker-prod: ${BACKEND_DIR}
	docker run -it -v "$(shell pwd)/backend:/usr/src/backend" -w /usr/src/backend -p 80:80 flask-docker

run-backend-tests: ${BACKEND_DIR}
	python3 backend/tests.py

run-frontend-tests: ${FRONTEND_DIR}
	npm test -- --watchAll=false

run-gui-tests: ${FRONTEND_DIR} # to be filled in

run-postman-tests: ${BACKEND_DIR} # to be filled in

clean: 
	rm -r frontend/build

CFILES := README.md \
			.gitlab-ci.yml \

check: ${CFILES}
	@echo 'All files exist'


	