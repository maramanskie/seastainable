import { render, screen } from '@testing-library/react';
import Home from './pages/Home';
import NavBar from './pages/NavBar';
import Util from './pages/utils';
import Ingredients from './pages/Ingredients'
import Recipes from './pages/Recipes';
import MarineLife from './pages/MarineLife';
import { BrowserRouter } from "react-router-dom";

describe("Util Functions Have Expected Behavior", () => {

  test("Util's checkOrEx true is check emoji", () => {
    expect(Util.checkOrEx(true)).toBe('✔️');
  });

  test("Util's checkOrEx false is ex emoji", () => {
    expect(Util.checkOrEx(false)).toBe('❌');
  });

  test("replacePythonBoolWithJSBool Pythonic True replaces JS true", () => {
    let input = '{"willItWork": True}';
    let expected = '{"willItWork": true}'
    let actual = Util.replacePythonBoolWithJSBool(input);
    expect(expected).toBe(actual);
  });

  test("replacePythonBoolWithJSBool Pythonic True replaces JS true multiple instances", () => {
    let input = '{"willItWork1": True, "willItWork2": True, "willItWork3": True}';
    let expected = '{"willItWork1": true, "willItWork2": true, "willItWork3": true}';
    let actual = Util.replacePythonBoolWithJSBool(input);
    expect(expected).toBe(actual);
  });

  test("replacePythonBoolWithJSBool Pythonic False replaces JS false multiple instances", () => {
    let input = '{"willItWork1": False, "willItWork2": False, "willItWork3": False}';
    let expected = '{"willItWork1": false, "willItWork2": false, "willItWork3": false}';
    let actual = Util.replacePythonBoolWithJSBool(input);
    expect(expected).toBe(actual);
  });

  test("replaceJSONSingleQuoteWithDoubleQuote does what it says", () => {
    let input = "{'dairyFree': False, 'glutenFree': False, 'vegan': False, 'vegetarian': False}";
    let expected = `{"dairyFree": False, "glutenFree": False, "vegan": False, "vegetarian": False}`;
    let actual = Util.replaceJSONSingleQuoteWithDoubleQuote(input);
    expect(expected).toBe(actual);
  })

  test("getPercentage gets numeric value from string", () => {
    let input = "12.34%";
    let expected = 12.34;
    let actual = Util.getPercentage(input);
    expect(expected).toBe(actual);
  })

});

describe('Front End Components that are expected exist in the document', () => {
  
  test('Home Screen contains Welcome Message', () => {
    <BrowserRouter>
      render(<Home />);
      const linkElement = screen.getByDisplayValue("Welcome to Seastainable!");
      expect(linkElement).toBeInTheDocument();
    </BrowserRouter>
  });
  
  test('Ingredients view has desired attributes', () => {
    const name = "Ingredient Name";
    const attributes = ["Price per Serving", "Aisle", "Consistency", "Fat", "Carbohydrates", "Protein"];
  
    <BrowserRouter>
      render(<Ingredients />);
      expect(screen.getByDisplayValue("Price per Serving")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Aisle")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Consistency")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Fat")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Carbohydrates")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Protein")).toBeInTheDocument();
      {/* {attributes.forEach(attribute =>
        expect(screen.getByDisplayValue(attribute)).toBeInTheDocument()
      )} */}
    </BrowserRouter>
  })

  test('Recipes view has desired attributes', () => {
    const name = "Recipe Name";
    const attributes = ["Cost", "Rating", "Preparation Time", "Servings", "Dairy Free?", "Gluten Free?", "Vegan?", "Vegetarian?"];
    
    <BrowserRouter>
      render(<Ingredients />);
      expect(screen.getByDisplayValue(name)).toBeInTheDocument();
      expect(screen.getByDisplayValue("Cost")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Rating")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Preparation Time")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Servings")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Dairy Free?")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Gluten Free?")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Vegan?")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Vegetarian?")).toBeInTheDocument();
      {/* {attributes.forEach(attribute =>
        expect(screen.getByDisplayValue(attribute)).toBeInTheDocument()
      )} */}
    </BrowserRouter>
  })

  test('MarineLife view has desired attributes', () => {
    const name = "Marine Life Common Name";
    const attributes = ["Scientific Name", "Harvest Type", "Availability", "Region", "Water Body", "Calories per serving"];
    
    <BrowserRouter>
      render(<Ingredients />);
      expect(screen.getByDisplayValue(name)).toBeInTheDocument();
      expect(screen.getByDisplayValue("Scientific Name")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Harvest Type")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Availability")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Region")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Water Body")).toBeInTheDocument();
      expect(screen.getByDisplayValue("Calories per serving")).toBeInTheDocument();

      {/* {attributes.forEach(attribute =>
        expect(screen.getByDisplayValue(attribute)).toBeInTheDocument()
      )} */}
    </BrowserRouter>
  })
})


