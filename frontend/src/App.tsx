import { BrowserRouter, Navigate, Routes, Route } from "react-router-dom";
import './App.css';
import Home from './pages/Home';
import About from './pages/About';
import MarineLife from './pages/MarineLife';
import MarineLifeInstance from './pages/MarineLifeInstance';
import Recipes from './pages/Recipes';
import RecipeInstance from './pages/RecipeInstance';
import Ingredients from './pages/Ingredients';
import IngredientInstance from './pages/IngredientInstance';
import GlobalSearch from './pages/GlobalSearch';
import ProviderVisualizations from "./pages/ProviderVisualizations";
// import Visualizations from './pages/Visualizations/Data/IngredientAisleBubbles';
import Visualizations from './pages/DataVisualizations';


function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/home' element={<Navigate replace to="/" />} />
        <Route path='/' element={<Home />} />
        <Route path='/about' element={<About />} />

        <Route path='/marinelife' element={<MarineLife />} />
        <Route path='/marinelife/:id' element={<MarineLifeInstance />} />

        <Route path='/recipes' element={<Recipes />} />
        <Route path='/recipes/:id' element={<RecipeInstance />} />

        <Route path='/ingredients' element={<Ingredients />} />
        {/* <Route path='/ingredients?search=search' element={<Ingredients />} /> */}
        <Route path='/ingredients/:id' element={<IngredientInstance />} />

        <Route path='/visualizations' element={<Visualizations />} />

        <Route path='/global/' element={<GlobalSearch />} />
        <Route path='/global/:initialQuery' element={<GlobalSearch />} />

        <Route path='providervisualizations' element={<ProviderVisualizations />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
