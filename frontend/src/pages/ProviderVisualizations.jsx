import NavBar from "./NavBar";
import { useState, useEffect } from "react";
import { BarChart, Bar, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie, Sector } from 'recharts';
import axios from "axios";
import { LinearProgress } from "@mui/material";
import './Visualizations.css'

const BASEURL = "https://api.diminishingdisasters.me";
  
function LineChartDisasters() {
    const [floodData, setFloodData] = useState(null);
    const [cycloneData, setCycloneData] = useState(null);
    const [earthquakeData, setEarthquakeData] = useState(null);
    const [plottingData, setPlottingData] = useState(null);

    async function getDisasterDataByTypeAndMonth(type, month) {
        return axios.get(
            BASEURL + "/disasters?type=" + encodeURIComponent(type) + "&month=" + encodeURIComponent(month)
          ).then(response => {
            return response.data.total;
          });
    }

    async function getDisasterDataByType(type) {
        var data = [];
        let months = ["January", "February", "March", "April", "May", "June", "July", 
                    "August", "September", "October", "November", "December"];
        for(let i = 0; i < months.length; i++) {
            let totalForMonth = await getDisasterDataByTypeAndMonth(type, months[i]);
            data.push({month: months[i], count: totalForMonth});
        }

        return data;
    }

    function getPlottingData(floodData, cycloneData, earthquakeData) {
        if(floodData === null || cycloneData === null || earthquakeData === null) {
          return;
        }

        var finalData = [];
        let months = ["January", "February", "March", "April", "May", "June", "July", 
                      "August", "September", "October", "November", "December"];

        for(let i = 0; i < months.length; i++) {
          finalData.push({month: months[i], flood: floodData[i]['count'], 
                          cyclone: cycloneData[i]['count'], earthquake: earthquakeData[i]['count']});
        }

        setPlottingData(finalData);    
    }

    useEffect(() => {
        async function fetchFloodData() {
            if(floodData !== null) {
              return;
            }

            getDisasterDataByType("Flood")
              .then((data) => {
                setFloodData(data);
              })
              .catch((error) => {
                console.log(error);
              });
        }
        fetchFloodData();
    });

    useEffect(() => {
        async function fetchCycloneData() {
            if(cycloneData !== null) {
              return;
            }

            getDisasterDataByType("Tropical Cyclone")
              .then((data) => {
                setCycloneData(data);
              })
              .catch((error) => {
                console.log(error);
              });
        }
        fetchCycloneData();
    });

    useEffect(() => {
      async function fetchEarthquakeData() {
          if(earthquakeData !== null) {
              return;
          }

          getDisasterDataByType("Earthquake")
              .then((data) => {
                setEarthquakeData(data);
              })
              .catch((error) => {
                console.log(error);
              });
      }
      fetchEarthquakeData();
    });

    useEffect(() => {
        if(floodData === null || cycloneData === null || earthquakeData === null || plottingData !== null) {
            return;
        }

        getPlottingData(floodData, cycloneData, earthquakeData);
    });

    if(plottingData === null) {
        return (
            <div style={{width: "100%", height: 750}}>
                <LinearProgress />
            </div>
        );
    }

    return (
        <div>
            <LineChart
              width={1200}
              height={400}
              data={plottingData}
              margin={{
                top: 20,
                right: 20,
                left: 20,
                bottom: 20,
              }}>
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="month" label={{ value: 'Month', angle: 0, position: 'insideBottom' }} />
                  <YAxis label={{ value: 'Frequency', angle: -90, position: 'left' }} />
                  <Tooltip />
                  <Legend />
                  <Line type="monotone" dataKey="flood" stroke="#8884d8" activeDot={{ r: 8 }} />
                  <Line type="monotone" dataKey="cyclone" stroke="#82ca9d" />
                  <Line type="monotone" dataKey="earthquake" stroke="#ff0000" />
            </LineChart>
        </div>
    );
}

function DisasterTypePieChart() {
  const [pieChartData, setPieChartData] = useState(null);
  const [activeIndex, setActiveIndex] = useState(0);

  async function getDisasterDataType(type) {
      return axios.get(
        BASEURL + "/disasters?type=" + encodeURIComponent(type)
      ).then(response => {
        return response.data.total;
      });
  }

  async function getPieChartData() {
      var data = [];
      let types =  [
        "Tropical Cyclone",
        "Flash Flood",
        "Flood",
        "Wild Fire",
        "Volcano",
        "Earthquake",
        "Epidemic",
        "Technological Disaster",
        "Insect Infestation",
        "Drought",
        "Cold Wave",
        "Severe Local Storm",
        "Tsunami",
        "Heat Wave",
        "Mud Slide",
        "Extratropical Cyclone",
        "Fire",
        "Land Slide",
        "Other",
        "Snow Avalanche",
        "Storm Surge"
      ];

      for(let i = 0; i < types.length; i++) {
        let typeTotal = await getDisasterDataType(types[i]);
        data.push({name: types[i], value: typeTotal});
      }

      return data;
  }

  useEffect(() => {
      async function fetchPieChartData() {
          if(pieChartData !== null) {
            return;
          }

          getPieChartData()
            .then((data) => {
              setPieChartData(data);
            })
            .catch((error) => {
              console.log(error);
            });
        }
        fetchPieChartData();
  });
  
  const renderActiveShape = (props) => {
      const RADIAN = Math.PI / 180;
      const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
      const sin = Math.sin(-RADIAN * midAngle);
      const cos = Math.cos(-RADIAN * midAngle);
      const sx = cx + (outerRadius + 10) * cos;
      const sy = cy + (outerRadius + 10) * sin;
      const mx = cx + (outerRadius + 30) * cos;
      const my = cy + (outerRadius + 30) * sin;
      const ex = mx + (cos >= 0 ? 1 : -1) * 22;
      const ey = my;
      const textAnchor = cos >= 0 ? 'start' : 'end';
    
      return (
          <g>
              <text x={cx} y={cy} dy={8} fontSize="50px" textAnchor="middle" fill={fill}>
                  {payload.name}
              </text>
              <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill={fill}
              />
              <Sector
                cx={cx}
                cy={cy}
                startAngle={startAngle}
                endAngle={endAngle}
                innerRadius={outerRadius + 6}
                outerRadius={outerRadius + 10}
                fill={fill}
              />
              <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
              <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
              <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} fontSize="20px" textAnchor={textAnchor} fill="#333">{`Frequency ${value}`}</text>
              <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} fontSize="20px" textAnchor={textAnchor} fill="#999">
                {`(Rate ${(percent * 100).toFixed(2)}%)`}
              </text>
          </g>
      );
  };    

  let onPieEnter = (_, index) => {
      setActiveIndex(index);
  };

  if(pieChartData === null) {
      return (
          <div style={{width: "100%", height: 750}}>
              <LinearProgress />
          </div>
      );
  }

  return (
      <div>
          <PieChart width={1000} height={750}>
              <Pie
                activeIndex={activeIndex}
                activeShape={renderActiveShape}
                data={pieChartData}
                cx="50%"
                cy="50%"
                innerRadius={300}
                outerRadius={320}
                fill="#8884d8"
                dataKey="value"
                onMouseEnter={onPieEnter}
              />
        </PieChart>
      </div>
  );
}

function OrganizationAssistBarChart() {

    const [barGraphData, setBarGraphData] = useState(null);

    async function getTotalOrganizations() {
        return axios.get(
          BASEURL + "/organizations"
        ).then(response => {
          return response.data.total;
        });
    }

    async function getOrganizationDataForPage(page) {
        return axios.get(
          BASEURL + "/organizations?page=" + encodeURIComponent(page)
        ).then(response => {
          return response.data.organizations;
        });
    }

    async function getOrganizationData() {
        let total = await getTotalOrganizations();
        var numPages = Math.floor(total / 10);
        if(total % 10 !== 0) {
            numPages = numPages + 1;
        }

        var data = [];

        for(let i = 0; i < numPages; i++) {
            let pageData = await getOrganizationDataForPage(i + 1);
            for(let j = 0; j < pageData.length; j++) {
                data.push(pageData[j]['score']);
            }
        }

        return data;
    }

    function findBin(number, thresholds) {
        // Bins
        // Lower exclusive, upper inclusive
        // 86 - 88
        // 88 - 90
        // 90 - 92
        // 92 - 94
        // 94 - 96
        // 96 - 98
        // 98 - 100
        let i = 0;
        while(i < thresholds.length && number > thresholds[i]) {
          i++;
        }
        return i;
    }

    function binData(data) {
        var binnedData = [{name: '86-88', freq: 0}, {name: '88-90', freq: 0}, {name: '90-92',freq: 0}, 
                          {name: '92-94', freq: 0}, {name: '94-96', freq: 0}, {name: '96-98', freq: 0}, 
                          {name: '98-100', freq: 0}];
        let thresholds = [88, 90, 92, 94, 96, 98, 100];

        for(let i = 0; i < 194; i++) {
            let bin = findBin(data[i], thresholds);
            binnedData[bin].freq = binnedData[bin].freq + 1;
        }
        return binnedData;
    }

    useEffect(() => {
        async function fetchBarGraphData() {
            if(barGraphData !== null) {
                return;
            }

            getOrganizationData()
              .then((data) => {
                let binnedData = binData(data);
                setBarGraphData(binnedData);
              })
              .catch((error) => {
                console.log(error);
              });
        }
        fetchBarGraphData();
    });

    if(barGraphData === null) {
        return (
            <div style={{width: "100%", height: 400}}>
                <LinearProgress />
            </div>
        );
    }

    return (
        <div>
            <BarChart
              width={1000}
              height={400}
              data={barGraphData}
              margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 20,
              }}
              barCategoryGap={0}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" label={{ value: 'Score Range', angle: 0, position: 'bottom'}} />
                <YAxis label={{ value: '# of Organizations', angle: -90, position: 'insideLeft'}}/>
                <Tooltip />
                <Bar dataKey="freq" fill="#8884d8" />
            </BarChart>
        </div>
    );
}

function ProviderVisualizations() {
    return (
        <div>
            <NavBar />
            <h1 id="visualTitle">Frequency of Disasters for Each Month for Top Three Disaster Types</h1>
            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <LineChartDisasters />
            </div>
            <h1 id="visualTitle">Distribution of Disasters By Type</h1>
            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <DisasterTypePieChart />
            </div>
            <h1 id="visualTitle">Distribution of Scores for International Organizations</h1>
            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <OrganizationAssistBarChart />
            </div>
        </div>
    );
}

export default ProviderVisualizations;