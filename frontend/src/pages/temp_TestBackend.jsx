// This can be removed.
// A simple proof of concept to demonstrate what is necessary to talk to the backend from the frontend.

import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function Devs() {
  const [devs, setDevs] = useState([]);

  useEffect(() => {
    fetch('http://127.0.0.1:5000/devs')
      .then(res => res.json())
      .then(
        (response) => {
          // for this dummy hardcoded case, we just have an array, no need to parse it
          console.log(response);
          setDevs(response);
        });
}, [])

  return (
    <div>
      { "Brought to you by: " + devs.join(", ") }
    </div>
  );
}

function PageIds() {
  const [marineLifePage, setMarineLifePage] = useState("");
  const { pageNum } = useParams();

  useEffect(() => {
    fetch('http://127.0.0.1:5000/marinelife/page/' + pageNum)
      .then(res => res.json())
      .then(
        (response) => {
          // for this dummy hardcoded case, we just have an array, no need to parse it
          console.log(response);
          setMarineLifePage(response);
        });
  }, [pageNum])

  return (
    <div>
      { JSON.stringify(marineLifePage) }
    </div>
  );
}

function InstanceById() {
  const [marineLifePage, setMarineLifePage] = useState("");
  const { id } = useParams();

  useEffect(() => {
    fetch('http://127.0.0.1:5000/marinelife/id/' + id)
      .then(res => res.json())
      .then(
        (response) => {
          // for this dummy hardcoded case, we just have an array, no need to parse it
          console.log(response);
          setMarineLifePage(response);
        });
  }, [id])

  return (
    <div>
      { JSON.stringify(marineLifePage) }
    </div>
  );
}

function InstanceByName() {
  const [marineLifePage, setMarineLifePage] = useState("");
  const { name } = useParams();

  useEffect(() => {
    fetch('http://127.0.0.1:5000/marinelife/name/' + name)
      .then(res => res.json())
      .then(
        (response) => {
          // for this dummy hardcoded case, we just have an array, no need to parse it
          console.log(response);
          setMarineLifePage(response);
        });
  }, [name])

  return (
    <div>
      { JSON.stringify(marineLifePage) }
    </div>
  );
}

function AllInstances() {
  const [marineLifePage, setMarineLifePage] = useState("");

  useEffect(() => {
    fetch('http://127.0.0.1:5000/marinelife/all')
      .then(res => res.json())
      .then(
        (response) => {
          // for this dummy hardcoded case, we just have an array, no need to parse it
          console.log(response);
          setMarineLifePage(response);
        });
  }, [])

  return (
    <div>
      { JSON.stringify(marineLifePage) }
    </div>
  );
}

export {Devs, PageIds, AllInstances, InstanceById, InstanceByName};