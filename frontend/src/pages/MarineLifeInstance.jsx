import NavBar from './NavBar'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useState, useEffect } from 'react'
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import { BACKEND_ENDPOINT } from './env';
import { LinearProgress } from '@mui/material';

var cardStyle = {
  width: '900px', 
  height: '100%',
  marginTop: '20px'
}

var recipeImage = {
  height: 'auto',
  maxHeight: '350px',
  width: 'auto',
  maxWidth: '350px',
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto'
}

var cardLabels = {
  fontWeight: 'bold',
  fontSize: '17px',
  marginTop: '25px'

}

var dividerLabels = {
  fontWeight: 'bold',
  fontSize: '20px'
  
}

let mapsAPIBase = "https://www.google.com/maps/embed/v1/place?key=AIzaSyAqfd5uwYVvATapVyQrHA25_D6hTkFwpP8&zoom=10&maptype=satellite&q=";
async function getIDForIngredient(ingName) {
  return axios.get(
    BACKEND_ENDPOINT + "/ingredients/name/" + encodeURIComponent(ingName)
  ).then(response => {
    let id = (response.data && response.data[0] && response.data[0].id) ? response.data[0].id : -1;
    return id;
  })
}

async function getIDForRecipe(recipeName) {
  return axios.get(
    BACKEND_ENDPOINT + "/recipes/name/" + encodeURIComponent(recipeName)
  ).then(response => {
    let id = (response.data && response.data[0] && response.data[0].id) ? response.data[0].id : -1;
    console.log(id);
    return id;
  })
}

// Recipe instance
function MarineLifeInstance() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [data, setData] = useState({});
  const [ingredients, setIngredients] = useState([]);
  const [recipes, setRecipes] = useState([]);

  useEffect(() => {
      // get recipe data
    const fetchMarineLife = async() => {
      try {
        let response = await axios.get(
          BACKEND_ENDPOINT + "/marinelife/id/" + id
        );
        setData(response.data[0]);
        let ings = [];
        await Promise.all( 
          JSON.parse(response.data[0].ingredients).map(async (ingName) => {
            let promisedID = await getIDForIngredient(ingName);
            let id = Number(promisedID);
            ings.push({ingName, id});
          })
        );
        setIngredients(ings);

        let recipes = [];
        await Promise.all( 
          JSON.parse(response.data[0].recipes).map(async (recipeName) => {
            let promisedID = await getIDForRecipe(recipeName);
            let id = Number(promisedID);
            recipes.push({recipeName, id});
          })
        );
        console.log(recipes);
        setRecipes(recipes);

      } catch (error) {
        console.error(error);
      }
    };

    fetchMarineLife();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  return (
    <div>
    <NavBar></NavBar>

    <div style={{
      display: "flex",
      flexDirection: "column",
      alignItems: "center"
    }}>
      <Card sx={{ maxWidth: 900 }} style={cardStyle} >
        

        <CardContent>
        <Button variant="contained" color="primary" onClick={() => navigate(-1)}>
        <ArrowBackIcon></ArrowBackIcon>
        </Button>

        <CardMedia
          component="img"
          height="340"
          width="20"
          image={data.image}
          alt={`${data.name}`}
          style={recipeImage}
        />

          <Typography gutterBottom variant="h4" component="div" style={{ textAlign: 'center', fontWeight: 'bold' }}>
            {data.name}
          </Typography>


          <Divider><div style={dividerLabels}>Marine Life Information</div></Divider>

          { data.locationMap ?
            <Typography variant="body1">
            <div><strong>Common Name:</strong> {data.commonName}</div> <br/>
            <div><strong>Scientific Name:</strong> {data.scientificName}</div><br/>
            <div><strong>Resident Body of Water:</strong> {data.waterBody}</div><br/>
            <div><strong>Regions of residence:</strong> {data.regions}</div><br/>
            <div><strong>Harvest Type:</strong> {data.harvestType}</div><br/>
            <div><strong>Identifying Features:</strong> <span dangerouslySetInnerHTML={{__html: data.idFeatures}}></span></div>
            <div><strong>Population Status: </strong> <span dangerouslySetInnerHTML={{__html: data.populationStatus}}></span></div>
            <div><strong>Availability: </strong> <span dangerouslySetInnerHTML={{__html: data.availability}}></span></div><br/>
            <div><strong>Info: </strong> <span dangerouslySetInnerHTML={{__html: data.quote}}></span></div><br/>
            <div><strong>Calories per serving:</strong> {data.calories}</div>
            </Typography>
            : <div>Loading information...<LinearProgress /></div>
          }

          <div style={cardLabels}>Map Location:</div> 
          {data.locationMap ? 
            <iframe
              title="Fish Location Map"
              width="450"
              height="250"
              frameBorder="0"
              src={mapsAPIBase + data.locationMap}>
            </iframe>
            : <div>Loading map...<LinearProgress /></div>
          }

          <Divider><div style={dividerLabels}>Recipes this Marine Species appears in</div></Divider>

          { data.recipes && recipes !== [] ? 
              recipes.map(item => 
                item.id !== -1 ? <span><Button href={`/recipes/${item.id}`}>{item.recipeName}</Button>, </span>  : item.recipeName + ", "
              )
             : <div>Loading connections...<LinearProgress /></div>
          }

          <Divider><div style={dividerLabels}>Ingredients this Marine Species appears alongside in Recipes</div></Divider>

          { data.ingredients && ingredients !== [] ? 
              ingredients.map(item => 
                item.id !== -1 ? <span><Button href={`/ingredients/${item.id}`}>{item.ingName}</Button>, </span>  : item.ingName + ", "
              )
             : <div>Loading connections...<LinearProgress /></div>
          }


        </CardContent>

      </Card>
    </div>
  </div>);
}

export default MarineLifeInstance;