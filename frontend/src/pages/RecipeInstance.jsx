import NavBar from './NavBar'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useState, useEffect } from 'react'
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import Utils from "./utils";
import { BACKEND_ENDPOINT } from './env';
import { LinearProgress } from '@mui/material';

var cardStyle = {
  width: '100%', 
  height: '100%',
  marginTop: '20px'
}

var recipeImage = {
  height: 'auto',
  maxHeight: '350px',
  width: 'auto',
  maxWidth: '350px',
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto'
}

var dividerLabels = {
  fontWeight: 'bold',
  fontSize: '20px'
}

async function getIDForIngredient(ingName) {
  return axios.get(
    BACKEND_ENDPOINT + "/ingredients/name/" + encodeURIComponent(ingName)
  ).then(response => {
    let id = (response.data && response.data[0] && response.data[0].id) ? response.data[0].id : -1;
    return id;
  })
}

async function getIDForMarineLife(marineLifeName) {
  // can have func for this
  // console.log(BACKEND_ENDPOINT + "/marinelife/name/" + encodeURIComponent(marineLifeName))
  return axios.get(
    BACKEND_ENDPOINT + "/marinelife/name/" + encodeURIComponent(marineLifeName)
  ).then(response => {
    // can have a func for extract id
    let id = (response.data && response.data[0] && response.data[0].id) ? response.data[0].id : -1;
    return id;
  })
}

// Recipe instance
function RecipeInstance() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [data, setData] = useState({});
  const [ingredients, setIngredients] = useState([]);
  const [marineLife, setMarineLife] = useState([]);
  const [instructions, setInstructions] = useState("Loading instructions..."); // either iframe or loading or not available.

  useEffect(() => {
      // get recipe data
    const fetchRecipe = async() => {
      try {
        let response = await axios.get(
          BACKEND_ENDPOINT + "/recipes/id/" + id
        );
        setData(response.data[0]);

        let ings = [];
        await Promise.all( 
          JSON.parse(response.data[0].ingredients).map(async (ingName) => {
            let promisedID = await getIDForIngredient(ingName);
            let id = Number(promisedID);
            ings.push({ingName, id});
          })
        );
        setIngredients(ings);

        let ml = [];
        await Promise.all( 
          JSON.parse(response.data[0].fishType).map(async (mlName) => {
            let promisedID = await getIDForMarineLife(mlName);
            let id = Number(promisedID);
            ml.push({mlName, id});
          })
        );
        console.log(ml);
        setMarineLife(ml);

        try {
          let instructionPageHeaders = (await axios.get(response.data[0].instructions,
            {
              method: 'GET',
              headers: {'Access-Control-Allow-Origin': '*'},
              // redirect: 'follow',
              mode: 'cors',
            })).headers;
          console.log(instructionPageHeaders);
          setInstructions(""); // good to go
        } catch (error) {
          console.log("Can't embed iframe: ", error);
          setInstructions("Sorry, instructions could not be embedded.");
        }        
      } catch (error) {
        console.error(error);
      }
    };

    fetchRecipe();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
    <NavBar></NavBar>

    <div style={{
      display: "flex",
      flexDirection: "column",
      alignItems: "center"
    }}>
      <Card sx={{ maxWidth: 900 }} style={cardStyle} >
        

        <CardContent>
        <Button variant="contained" color="primary" onClick={() => navigate(-1)}>
        <ArrowBackIcon></ArrowBackIcon>
        </Button>

        <CardMedia
          component="img"
          height="340"
          width="20"
          image={data.image}
          alt={`${data.name} recipe`}
          style={recipeImage}
        />

          <Divider><div style={dividerLabels}>Recipe Information</div></Divider>
          
          {instructions !== 'Loading instructions...' ? 
          <div>
          <Typography gutterBottom variant="h4" component="div" style={{ textAlign: 'center', fontWeight: 'bold' }}>
            {data.name}
          </Typography>

          <Typography variant="body1">
          <div><strong>Rating:</strong> {data.rating}</div> <br/>
          <div><strong>Prep Time:</strong> {data.prepTime}</div> <br/>
          <div><strong>Servings:</strong> {data.numServings}</div> <br/>
          <div><strong>Number of Ingredients:</strong> {ingredients.length}</div> <br/>

          <div><strong>Dietary Information:</strong></div><br/>
            Dairy Free: {data.diet ? Utils.checkOrEx(Utils.convertOurJSONStringToValidJSON(data.diet).dairyFree) : "" } <br/>
            Gluten Free: {data.diet ? Utils.checkOrEx(Utils.convertOurJSONStringToValidJSON(data.diet).glutenFree) : "" } <br/>
            Vegan: {data.diet ? Utils.checkOrEx(Utils.convertOurJSONStringToValidJSON(data.diet).vegan) : "" } <br/>
            Vegetarian: {data.diet ? Utils.checkOrEx(Utils.convertOurJSONStringToValidJSON(data.diet).vegetarian) : "" } <br/>


          </Typography>

          <div><strong>Instructions: </strong>
          View the recipe <Button size='small' href={data.instructions}>here!</Button>
          {
            instructions === "" 
            ? <span> (or below)<iframe src={data.instructions} width="850px" height="500px" title="Recipe Website Embed"></iframe></span>
            : <span> {instructions}</span>
          }
          </div>
         
          </div>
          : <div>Loading information...<LinearProgress /></div>
        }

          <Divider><div style={dividerLabels}>Marine Life that may be used in this Recipe</div></Divider>

          <Typography variant="body1">
          
          { data.fishType && marineLife !== [] ? 
              marineLife.map(item => 
                item.id !== -1 ? <Button href={`/marinelife/${item.id}`}>{item.mlName}, </Button>  : item.mlName + ", "
              )
             : <div>Loading connections...<LinearProgress /></div>
          }
          </Typography>

          <Divider><div style={dividerLabels}>Ingredients used in this Recipe</div></Divider>
          <Typography variant="body1">
          { data.ingredients && ingredients !== [] ? 
              ingredients.map(item => 
                item.id !== -1 ? <Button href={`/ingredients/${item.id}`}>{item.ingName}, </Button>  : item.ingName + ", "
              )
             : <div>Loading connections...<LinearProgress /></div>
          }
          </Typography>


        </CardContent>

      </Card>
    </div>
  </div>);
}

export default RecipeInstance;