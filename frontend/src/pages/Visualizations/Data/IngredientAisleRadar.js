import { LinearProgress } from '@mui/material';
import { useState, useEffect } from 'react';
import RadarChartDisplay from '../Charts/RadarChart';

function processApiData(ingredientAisleData) {

  let aisleMap = new Map([
    ['Alcoholic Beverages', 0],
    ['Bakery/Bread', 0],
    ['Baking', 0],
    ['Canned and Jarred', 0],
    ['Cheese', 0],
    ['Condiments', 0],
    ['Ethnic Foods', 0],
    ['Meat', 0],
    ['Milk, Eggs, Other Dairy', 0],
    ['Oil, Vinegar, Salad Dressing', 0],
    ['Pasta and Rice', 0],
    ['Produce', 0],
    ['Seafood', 0],
    ['Spices and Seasonings', 0],
  ]);

  // Go through each
  for (let i = 0; i < ingredientAisleData.length; i++) {
    let curIngredient = ingredientAisleData[i].aisle;
    if (aisleMap.has(curIngredient)){
      let currCount = aisleMap.get(curIngredient)
      aisleMap.set(curIngredient, currCount + 1)
    }   


  }

  let processedData = [];
  for (const [key, val] of aisleMap){
      let curAisle ={
        _id: key,
        label: key,
        value: val,
    };
    processedData.push(curAisle);
  }

  console.log("Processed Data", processedData)

  return processedData;
}

function IngredientAisleProcessor() {

  // set state
  const [ingredientAisle, setIngredientAisle] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);

    const getIngredientData = async () => {
      if(ingredientAisle !== null) return;

      fetch('https://api.seastainable.me/ingredients?page=1&instancesPerPage=300')
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          let processedData = processApiData(data.data);
          setIngredientAisle(processedData);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    useEffect(getIngredientData);


  if (ingredientAisle === null && !errorFlag) {
    return <div><LinearProgress/></div>;
  } else if (errorFlag) {
    return <div>Error with API call</div>;
  }

  return <RadarChartDisplay data={ingredientAisle} />;
}

export default IngredientAisleProcessor;
