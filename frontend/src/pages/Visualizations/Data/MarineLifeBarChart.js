import { LinearProgress } from '@mui/material';
import { useState, useEffect } from 'react';
import ScatterChartDisplay from '../Charts/ScatterChart';

function processApiData(marineData) {

    let marineLifeMap = {}

    console.log("Marine Data", marineData)

  // Go through each
  for (let i = 0; i < marineData.length; i++) {
    let curMarineLife = marineData[i].commonName;
    let allRecipes = marineData[i].recipes
    let recipeCount = allRecipes.split("\",").length

    marineLifeMap[curMarineLife] = recipeCount;

  }

  console.log("List:", marineLifeMap)

  let processedData = [];
  var x = 0;
  for (const [key, val] of Object.entries(marineLifeMap)){
      let curMarineLife = {};
      curMarineLife['name'] = key;
      curMarineLife["recipes"] = val;
      curMarineLife["x"] = x

      x += 10;

    processedData.push(curMarineLife);
  }

  console.log("Processed Data", processedData)

  return processedData;
}

function MarineRecipeProcessor() {

  // set state
  const [marineRecipe, setMarineRecipe] = useState(null);
  const [errorFlag, setErrorFlag] = useState(false);

    const getMarineRecipe = async () => {
      if(marineRecipe !== null) return;

      fetch('https://api.seastainable.me/marinelife?page=1&instancesPerPage=300')
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          let processedData = processApiData(data.data);
          setMarineRecipe(processedData);
        })
        .catch((error) => {
          console.log(error);
          setErrorFlag(true);
        });
    };

    useEffect(getMarineRecipe);

  if (marineRecipe === null && !errorFlag) {
    return <div><LinearProgress /></div>;
  } else if (errorFlag) {
    return <div>Error with API call</div>;
  }

  return <ScatterChartDisplay data={marineRecipe} />;
}

export default MarineRecipeProcessor;
