import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer} from 'recharts';
import PropTypes from 'prop-types';


const CustomTooltip = ({ active, payload, label }) => {
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p className="label">{`recipes : ${payload[1].value}`}</p>
        <p className="name">{`name : ${payload[0].payload.name}`}</p>
        </div>
      );
    }

    return null;
};



function ScatterChartDisplay(props) {
    const data = props.data

    if(!data || !data.length) {
        return <div>Loading...</div>
    }

    console.log(data)

    return (
        <div style={{height: 1000}}>
            <ResponsiveContainer width="100%" height="100%">
            <ScatterChart
                width={500}
                height={300}
                margin={{
                top: 20,
                right: 20,
                bottom: 20,
                left: 20,
                }}
            >
                <CartesianGrid />
                <XAxis type="number" dataKey="x" tick = {false}/>
                <YAxis type="number" dataKey="recipes" name="number of recipes" />
                <Tooltip content={<CustomTooltip />}/>
                <Scatter name="A school" data={data} fill="#8884d8"/>
            </ScatterChart>
            </ResponsiveContainer>
        </div>
      );
      
}

export default ScatterChartDisplay;
  
ScatterChartDisplay.propTypes = {
    data: PropTypes.array,
    dataKey: PropTypes.any
};