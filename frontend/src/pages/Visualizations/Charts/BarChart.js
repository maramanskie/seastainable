import PropTypes from 'prop-types';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';



function BarChartDisplay(props) {
    const data = props.data;

    if(!data || !data.length) {
        return <div>Loading...</div>
    }

    console.log(data)

    return (
        <div style={{height: 1000}}>

            <ResponsiveContainer width="100%" height="100%">
            <BarChart
                width={500}
                height={300}
                data={data}
                margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="Num recipes" fill="#8884d8" />
                {/* <Bar dataKey="uv" fill="#82ca9d" /> */}
            </BarChart>
            </ResponsiveContainer>
        </div>
      );
}

export default BarChartDisplay;
  
BarChartDisplay.propTypes = {
    data: PropTypes.array,
    dataKey: PropTypes.string
};
  