import React from 'react';
import { PieChart, Pie, Tooltip, Cell, Legend, ResponsiveContainer } from 'recharts';
import PropTypes from 'prop-types';

function PieChartDisplay(props) {
  const { data, dataKey } = props;
  const outerRadius = 250;
  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#8884d8'];

  return (
    <ResponsiveContainer id="pie-chart">
      <PieChart align="center">
        <Pie
          dataKey={dataKey}
          data={data}
          cx="50%"
          cy="50%"
          outerRadius={outerRadius}
          fill="#0088FE"
          isAnimationActive
          label
          labelLine>
          {data.map((_, index) => {
            return <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />;
          })}
        </Pie>
        <Legend />
        <Tooltip />
      </PieChart>
    </ResponsiveContainer>
  );
}

PieChartDisplay.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.number
    })
  ),
  dataKey: PropTypes.string
};

export default PieChartDisplay;
