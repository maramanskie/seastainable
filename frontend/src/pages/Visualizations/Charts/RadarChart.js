import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer } from 'recharts';
import PropTypes from 'prop-types';


function RadarChartDisplay(props) {
    const data = props.data;

    if(!data || !data.length) {
        return <div>Loading...</div>
    }

    console.log(data)

    return (
        <div style={{height: 1000}}>
            <ResponsiveContainer width="100%" height="100%">
                <RadarChart cx="50%" cy="50%" outerRadius="80%" data={data}>
                    <PolarGrid />
                    <PolarAngleAxis dataKey="label" />
                    <PolarRadiusAxis />
                    <Radar name="Mike" dataKey="value" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
                </RadarChart>
            </ResponsiveContainer>
            {/* <div>{JSON.stringify(data)}</div> */}
        </div>
    )

  }
  
    export default RadarChartDisplay;
  
RadarChartDisplay.propTypes = {
    data: PropTypes.array
};

            