import NavBar from './NavBar'
import IngredientAisleRadar from './Visualizations/Data/IngredientAisleRadar'
import MarineLifeBarChart from './Visualizations/Data/MarineLifeBarChart'
import { PieChart, Pie, Tooltip } from 'recharts';
import axios from "axios";
import Utils from './utils';
import { BACKEND_ENDPOINT } from './env';
import { useState, useEffect } from 'react';
import './Visualizations.css'
import { LinearProgress } from '@mui/material';


function DataVisualizations() {
    const [data, setData] = useState({});

    useEffect(() => {
        const fetchIngredient = async() => {
            try {
              let response = await axios.get(
                BACKEND_ENDPOINT + "/ingredients/id/3"
              );
              setData(response.data[0]);
            } catch (error) {
              console.error(error);
            }
        }
        console.log(data);
        console.log(data === {});
        fetchIngredient();
        //eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div>
            <NavBar></NavBar>

            <div classname = "visualization" style={{margin: '2vw'}}>
            <h1>Visualization Data for Seastainable</h1>


                <h2 id='visualTitle'>Caloric Breakdown (percentage) for cabbage</h2>
                {data && Object.keys(data).length > 0 ?
                    <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    <PieChart width={400} height={300}>
                    <Pie dataKey="value" data={Utils.getCaloricBreakdownData(data)} cx="50%" cy="50%" innerRadius={40} outerRadius={80} fill="skyblue" />
                    <Tooltip />
                    </PieChart>
                    </div>
                    : <LinearProgress />
                }

                <h2 id='visualTitle'>Number of Ingredients found in certain Aisles</h2>
                <IngredientAisleRadar></IngredientAisleRadar>

                <h2 id='visualTitle'>Number of Recipes per Fish</h2>
                <MarineLifeBarChart></MarineLifeBarChart>
            </div>
            
        </div>
    )
}

export default DataVisualizations;