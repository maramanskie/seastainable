import NavBar from './NavBar'
import axios from "axios";
import { useState, useEffect } from 'react'
import { useTheme } from '@mui/material/styles';
import { Button, TextField, Box, IconButton, OutlinedInput, LinearProgress } from '@mui/material';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TablePagination from '@mui/material/TablePagination';
import { BACKEND_ENDPOINT } from './env';
import Utils from "./utils";
import { useSearchParams } from "react-router-dom";

const name = "Ingredient Name";
const attributes = ["Price per Serving", "Consistency", "Aisle", "% of Caloric Intake that is Fat"];

const filterableAttributes = {
  "Consistency is solid": "consistency=solid",
  "Consistency is liquid": "consistency=liquid", 
  "In Produce Aisle": "aisle=Produce",
  "In Seafood Aisle": "aisle=Seafood",
  "In Baking Aisle": "aisle=Baking",
  "In Condiments Aisle": "aisle=Condiments",
  "Price per Serving >= $1.00": "price=gte:100", // it's in cents
  "Price per Serving <= $1.00": "price=lte:100",
  "Caloric Intake Fat % <= 50%": "percentFat=lte:50.0",
  "Caloric Intake Fat % >= 50%": "percentFat=gte:50.0"
}

const sortableAttributes = ["None", "ingName", "price", "aisle", "consistency", "percentFat"];
const displaySortableAttributes = ["None", "Ingredient Name", "Price per Serving", "Aisle", "Consistency", "% of Caloric Intake that is Fat"];

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    console.log(count);
    console.log(rowsPerPage);
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

function getQueryString(currentPage, itemsPerPage, searchQuery, sortAttribute, isAscending, filterQueries) {
  return BACKEND_ENDPOINT + "/ingredients?" +
    `instancesPerPage=${itemsPerPage}&` +
    `page=${currentPage+1}&` + // 0 indexed
    // empty string has falsy value
    (searchQuery ? `search=${encodeURIComponent(searchQuery)}&` : "") + 
    (sortAttribute && sortAttribute !== "None" ? `sort=${sortAttribute}:${isAscending ? "asc" : "desc"}&` : "") +
    (filterQueries ? filterQueries.join("&") + "&" : "");
}

function Ingredients() {
  const [currentPage, setCurrentPage] = useState(0);
  const [currentItems, setCurrentItems] = useState([]);
  const [count, setCount] = useState([]); 
  const [loading, setLoading] = useState(true);

  const [sortAttribute, setSortAttribute] = useState("None");
  const [ascending, setAscending] = useState(true); // ascending by default
 
  const [filterAttributes, setFilterAttributes] = useState([]);

  const [searchBoxContents, setSearchBoxContents] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [itemsPerPage, setItemsPerPage] = useState(Utils.DEFAULT_ITEMS_PER_PAGE);

  const [searchParams] = useSearchParams();

  const fetchIngredients = async(pgNum, numPerPage, searchQ, sortAttr, isAscending, filterAttrs, firstLoad) => {
    try {
      let response = await axios.get(
        getQueryString(pgNum, numPerPage, searchQ, sortAttr, isAscending, filterAttrs)
      );
      setCurrentItems(response.data.data);
      setCount(response.data.count);
    } catch (error) {
        console.error(error);
    }

    setLoading(false);

    
    if (!firstLoad) {
      window.location.replace(`?page=${pgNum}&items=${numPerPage}&search=${searchQ}&sortOn=${sortAttr}&asc=${isAscending}&filters=${filterAttrs}`);
    }
  }

  // separate effect for on page load
  useEffect(() => {
    for (const entry of searchParams.entries()) {
      const [param, value] = entry;
      switch (param) {
        case 'search':
          // handle "edge cases", "", None, etc
          setSearchBoxContents(value);
          setSearchQuery(value);
          break;
        case 'page':
          setCurrentPage(parseInt(value));
          break;
        case 'items':
          setItemsPerPage(parseInt(value));
          break;
        case 'asc':
          setAscending(value);
          break;
        case 'filters':
          setFilterAttributes(value.split(','));
          break;
        case 'sortOn':
          if (value !== 'None') {
            setSortAttribute(value.split(','));
          }
          break;
        default:
          break;
      }
      console.log(param, value);
    }
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    // if dependency stuff works don't need this or ingredients state
    // fetchIngredients(currentPage, itemsPerPage, ingredientsState['search'], sortAttribute, ascending, filterAttributes);
    fetchIngredients(currentPage, itemsPerPage, searchQuery, sortAttribute, ascending, filterAttributes, true);
  }, [currentPage, itemsPerPage, searchQuery, sortAttribute, ascending, filterAttributes]);

  const handleChangePage = (event, newPageNumber) => {
    setCurrentPage(newPageNumber);
    fetchIngredients(newPageNumber, itemsPerPage, searchQuery, sortAttribute, ascending, filterAttributes, false);
  };

  const handleChangeItemsPerPage = (event, newItemsPerPage) => {
    newItemsPerPage = parseInt(event.target.value, Utils.DEFAULT_ITEMS_PER_PAGE);
    setItemsPerPage(newItemsPerPage);
    setCurrentPage(0); // reset to start

    fetchIngredients(0, newItemsPerPage, searchQuery, sortAttribute, ascending, filterAttributes, false);
  };

  const monitorSearchBoxText = (event) => {
    setSearchBoxContents(event.target.value);
    let code = (event.keyCode ? event.keyCode : event.which);
    if(code === Utils.ENTER_KEYCODE) {
        handleSearch();
    }
  }

  const handleSortAttrChange = (event) => {
    let newSortAttribute = event.target.value;
    setSortAttribute(newSortAttribute);
    setCurrentPage(0);

    fetchIngredients(0, itemsPerPage, searchBoxContents, newSortAttribute, ascending, filterAttributes, false);
  };

  const handleAscendingChange = (event) => {
    let newAscendingTruthVal = event.target.value;
    setAscending(newAscendingTruthVal);
    setCurrentPage(0);

    fetchIngredients(0, itemsPerPage, searchBoxContents, sortAttribute, newAscendingTruthVal, filterAttributes, false);
  };

  const handleFilterChange = (event) => {
    let newFilters = event.target.value;
    setFilterAttributes(newFilters);
    setCurrentPage(0);

    fetchIngredients(0, itemsPerPage, searchBoxContents, sortAttribute, ascending, newFilters, false);
  };

  const handleSearch = (event) => {
    setSearchQuery(searchBoxContents ? searchBoxContents : searchQuery);
    setCurrentPage(0);

    fetchIngredients(0, itemsPerPage, searchBoxContents, sortAttribute, ascending, filterAttributes, false);
  };

  const emptyRows = currentPage > 0 ? Math.max(0, (1 + currentPage) * itemsPerPage - count) : 0;

    return (
        <div>
            <NavBar></NavBar>
            <div style={{margin: '2vw'}}>
            <strong style={{fontSize: "200%"}}>Viewing All Ingredients 🍅</strong> (click a row!)
            
            <span style={{marginLeft: "10px"}}>
            <TextField size="small" id="outlined-basic" label="Search here" variant="outlined" onKeyUp={monitorSearchBoxText} style={{marginTop: '10px'}} />
            <Button variant="contained" onClick={handleSearch} style={{marginRight:'10px'}}>Search</Button>
            <strong>Current Search Query:</strong> {searchQuery === "" ? "(none)" : searchQuery}
            </span>

            <span style={{align: "right", float: "right"}}>
            {/* sort */}
            <FormControl size="small" style={{marginTop: '10px'}}>
              <InputLabel>Sort on</InputLabel>
              <Select
                defaultValue='None'
                value={sortAttribute}
                label="Sort on"
                onChange={handleSortAttrChange}
              >
                {sortableAttributes.map(attr => 
                  <MenuItem value={attr}>{displaySortableAttributes[sortableAttributes.indexOf(attr)]}</MenuItem>
                )}
              </Select>
            </FormControl>

            {/* asc/desc */}
            <FormControl minWidth='200' size="small" style={{marginTop: '10px'}}>
              <InputLabel>Sort order</InputLabel>
              <Select
                value={ascending}
                label="Sort order"
                onChange={handleAscendingChange}
              >
                <MenuItem value={true}>Ascending</MenuItem>
                <MenuItem value={false}>Descending</MenuItem>
              </Select>
            </FormControl>


            {/* filter */}
            <FormControl sx={{ m: 1, width: 300 }} size="small">
              <InputLabel>Filters</InputLabel>
              <Select
                multiple
                value={filterAttributes}
                onChange={handleFilterChange}
                input={<OutlinedInput label="Filters" />}
              >
                {
                  Object.entries(filterableAttributes)
                  .map( ([key, value]) => 
                    <MenuItem
                      key={value}
                      value={value}
                    >
                      {key}
                    </MenuItem>
                  )
                }
              </Select>
            </FormControl>
            </span>

            {loading && <LinearProgress style={{marginTop: '10px', marginLeft: '-10px'}} />}

            {/* MATERIAL UI CODE */}
            {!loading && 
            <div>
            <TableContainer component={Paper}>
              <Table sx={Utils.TableStyle} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell><strong>{name}</strong></TableCell>
                    {attributes.map((attribute) => 
                      <TableCell align="right"><strong>{attribute}</strong></TableCell>
                    )}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {currentItems
                    // .slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage)
                    .map((row) => (
                    <TableRow
                      key={row.name}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      onClick={ event => window.location.href=`ingredients/${row.id}` } // very important for id to be specified
                      style={{cursor:'pointer'}}
                    >
                      <TableCell component="th" scope="row" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.ingName, searchQuery)}} />
                      <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.price, searchQuery)}} />
                      <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.consistency, searchQuery)}} />
                      <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.aisle, searchQuery)}} />
                      <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.percentFat, searchQuery)}} />
                    </TableRow>
                  ))}
                  {/* padding for empty rows on the last page */}
                  {emptyRows > 0 && (
                    <TableRow
                      style={{
                        height: 53 * emptyRows,
                      }}
                    >
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[5, 10, 15, 20, 25]}
              colSpan={3}
              count={count}
              rowsPerPage={itemsPerPage}
              page={currentPage}
              SelectProps={{
                inputProps: {
                  'aria-label': 'rows per page',
                },
                native: true,
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeItemsPerPage}
              ActionsComponent={TablePaginationActions}
            />
            </div>
            }
            </div>
        </div>
    );
}

export default Ingredients;