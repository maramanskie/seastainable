import NavBar from './NavBar'
import axios from "axios";
import { useState, useEffect } from 'react'
import { useTheme } from '@mui/material/styles';
import { Button, TextField, Box, IconButton } from '@mui/material';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import TablePagination from '@mui/material/TablePagination';
import { BACKEND_ENDPOINT } from './env';
import Utils from "./utils";
import { useParams, useSearchParams } from "react-router-dom";
import Grid from '@mui/material/Grid';


const nameM = "Marine Life Common Name";
const attributesM = ["Scientific Name", "Harvest Type", "Population Status", "Calories per serving"];

const nameI = "Ingredient Name";
const attributesI = ["Price per Serving", "Consistency", "Aisle", "% of Caloric Intake that is Fat"];

const nameR = "Recipe Name";
const attributesR = ["Preview Image", "Cost", "Rating", "Preparation Time", "Servings"];

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

function getQueryString(model, currentPage, itemsPerPage, searchQuery, sortAttribute, isAscending, filterQueries) {
  return BACKEND_ENDPOINT + `/${model}?` +
    `instancesPerPage=${itemsPerPage}&` +
    `page=${currentPage+1}&` + // 0 indexed
    // empty string has falsy value
    (searchQuery ? `search=${encodeURIComponent(searchQuery)}&` : "") + 
    (sortAttribute && sortAttribute !== "None" ? `sort=${sortAttribute}:${isAscending ? "asc" : "desc"}&` : "") +
    (filterQueries ? filterQueries.join("&") + "&" : "");
    // ascending ? "asc" : "desc"
}

function GlobalSearch() {
  const { initialQuery } = useParams();

  const [currentPageM, setCurrentPageM] = useState(0);
  const [itemsPerPageM, setItemsPerPageM] = useState(Utils.DEFAULT_ITEMS_PER_PAGE);
  const [currentItemsM, setCurrentItemsM] = useState([]); // data
  const [countM, setCountM] = useState([]); 

  const [currentPageI, setCurrentPageI] = useState(0);
  const [itemsPerPageI, setItemsPerPageI] = useState(Utils.DEFAULT_ITEMS_PER_PAGE);
  const [currentItemsI, setCurrentItemsI] = useState([]); // data
  const [countI, setCountI] = useState([]); 

  const [currentPageR, setCurrentPageR] = useState(0);
  const [itemsPerPageR, setItemsPerPageR] = useState(Utils.DEFAULT_ITEMS_PER_PAGE);
  const [currentItemsR, setCurrentItemsR] = useState([]); // data
  const [countR, setCountR] = useState([]); 

  const [searchBoxContents, setSearchBoxContents] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  
  const [searchParams] = useSearchParams();

  const fetchMarineLife = async(pgNum, numPerPage, searchQ, firstLoad) => {
    try {
      let response = await axios.get(
        getQueryString("marinelife", pgNum, numPerPage, searchQ)
      );
      setCurrentItemsM(response.data.data);
      setCountM(response.data.count);
    } catch (error) {
      console.error(error);
    }

    if (!firstLoad) {
      window.location.replace(`/global/${searchQ}?pageM=${pgNum}&itemsM=${numPerPage}&pageI=${currentPageI}&itemsI=${itemsPerPageI}&pageR=${currentPageR}&itemsR=${itemsPerPageR}`); // &search=${initialQuery}
    }
  };

  const fetchIngredients = async(pgNum, numPerPage, searchQ, firstLoad) => {
    try {
      let response = await axios.get(
        getQueryString("ingredients", pgNum, numPerPage, searchQ)
      );
      setCurrentItemsI(response.data.data);
      setCountI(response.data.count);
    } catch (error) {
      console.error(error);
    }
    
    if (!firstLoad) {
      window.location.replace(`/global/${searchQ}?pageM=${currentPageM}&itemsM=${itemsPerPageM}&pageI=${pgNum}&itemsI=${numPerPage}&pageR=${currentPageR}&itemsR=${itemsPerPageR}`);
    }
  };

  const fetchRecipes = async(pgNum, numPerPage, searchQ, firstLoad) => {
    try {
      let response = await axios.get(
        getQueryString("recipes", pgNum, numPerPage, searchQ)
      );
      setCurrentItemsR(response.data.data);
      setCountR(response.data.count);
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
    
    if (!firstLoad) {
      window.location.replace(`/global/${searchQ}?pageM=${currentPageM}&itemsM=${itemsPerPageM}&pageI=${currentPageI}&itemsI=${itemsPerPageI}&pageR=${pgNum}&itemsR=${numPerPage}`);
    }
  };

  useEffect(() => {
    setSearchQuery(initialQuery ? initialQuery : "");

    for (const entry of searchParams.entries()) {
      const [param, value] = entry;
      switch (param) {
        case 'search':
          // handle "edge cases", "", None, etc
          setSearchBoxContents(value);
          setSearchQuery(value);
          break;
        case 'pageM':
          setCurrentPageM(parseInt(value));
          break;
        case 'itemsM':
          setItemsPerPageM(parseInt(value));
          break;
        case 'pageI':
          setCurrentPageI(parseInt(value));
          break;
        case 'itemsI':
          setItemsPerPageI(parseInt(value));
          break;
        case 'pageR':
          setCurrentPageR(parseInt(value));
          break;
        case 'itemsR':
          setItemsPerPageR(parseInt(value));
          break;
        default:
          break;
      }
    }

    // eslint-disable-next-line
  }, []);

    
  useEffect(() => {
    fetchMarineLife(currentPageM, itemsPerPageM, initialQuery, true);
    // eslint-disable-next-line
  }, [currentPageM, itemsPerPageM]);
    
  useEffect(() => {
    fetchIngredients(currentPageI, itemsPerPageI, initialQuery, true);
    // eslint-disable-next-line
  }, [currentPageI, itemsPerPageI]);

  useEffect(() => {
    fetchRecipes(currentPageR, itemsPerPageR, initialQuery, true);
    // eslint-disable-next-line
  }, [currentPageR, itemsPerPageR]);

  const handleChangePageM = (event, newPageNumber) => {
    setCurrentPageM(newPageNumber);
    fetchMarineLife(newPageNumber, itemsPerPageI, searchQuery, false);
  };

  const handleChangePageI = (event, newPageNumber) => {
    setCurrentPageI(newPageNumber);
    fetchIngredients(newPageNumber, itemsPerPageI, searchQuery, false);
  };

  const handleChangePageR = (event, newPageNumber) => {
    setCurrentPageR(newPageNumber);
    fetchRecipes(newPageNumber, itemsPerPageR, searchQuery, false);
  };

  const handleChangeItemsPerPageM = (event, newItemsPerPage) => {
    newItemsPerPage = parseInt(event.target.value, Utils.DEFAULT_ITEMS_PER_PAGE);
    setItemsPerPageM(newItemsPerPage);
    setCurrentPageM(0);

    fetchMarineLife(0, newItemsPerPage, searchQuery, false);
  };

  const handleChangeItemsPerPageI = (event, newItemsPerPage) => {
    newItemsPerPage = parseInt(event.target.value, Utils.DEFAULT_ITEMS_PER_PAGE);
    setItemsPerPageI(newItemsPerPage);
    setCurrentPageI(0);

    fetchIngredients(0, newItemsPerPage, searchQuery, false);
  };

  const handleChangeItemsPerPageR = (event, newItemsPerPage) => {
    newItemsPerPage = parseInt(event.target.value, Utils.DEFAULT_ITEMS_PER_PAGE);
    setItemsPerPageR(newItemsPerPage);
    setCurrentPageR(0);

    fetchRecipes(0, newItemsPerPage, searchQuery, false);
  };

  const monitorSearchBoxText = (event) => {
    setSearchBoxContents(event.target.value);
    let code = (event.keyCode ? event.keyCode : event.which);
    if(code === Utils.ENTER_KEYCODE) {
        handleSearch();
    }
  }

  const handleSearch = (event) => {
    setSearchQuery(searchBoxContents);
    setCurrentPageM(0);
    setCurrentPageI(0);
    setCurrentPageR(0);

    fetchMarineLife(0, itemsPerPageM, searchBoxContents, false);
    fetchIngredients(0, itemsPerPageI, searchBoxContents, false);
    fetchRecipes(0, itemsPerPageR, searchBoxContents, false);
  };

  const emptyRowsM = currentPageM > 0 ? Math.max(0, (1 + currentPageM) * itemsPerPageM - countM) : 0;
  const emptyRowsI = currentPageI > 0 ? Math.max(0, (1 + currentPageI) * itemsPerPageI - countI) : 0;
  const emptyRowsR = currentPageR > 0 ? Math.max(0, (1 + currentPageR) * itemsPerPageR - countR) : 0;

    return (
        <div>
            <NavBar></NavBar>
            <div style={{marginLeft: '10px'}}>
            <strong style={{fontSize: "200%"}}>Global <span style={{color: "blue"}}>SEA</span>rch for: {searchQuery ? searchQuery : "(none, displaying all)"} 🌎</strong> (click a row!)
            
            <span style={{marginLeft: "10px"}}>
            <TextField size="small" id="outlined-basic" label="Search here" variant="outlined" onKeyUp={monitorSearchBoxText} style={{marginTop: '10px'}} />
            <Button variant="contained" onClick={handleSearch} style={{marginRight:'10px'}}>Search</Button>
            </span>

            <Grid container spacing={2}>
              {/* MARINE LIFE */}
              <Grid item xs={4}>
                <TableContainer component={Paper}>
                <Table sx={Utils.TableStyle} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell><strong>{nameM}</strong></TableCell>
                      {attributesM.map((attribute) => 
                        <TableCell align="right"><strong>{attribute}</strong></TableCell>
                      )}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {currentItemsM
                      // .slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage)
                      .map((row) => (
                      <TableRow
                        key={row.commonName}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 }}}
                        onClick={ event => window.location.href=`/marinelife/${row.id}` } // very important for id to be specified
                        style={{cursor:'pointer'}}
                      >
                        <TableCell component="th" scope="row"  dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.commonName, searchQuery)}}/>
                        <TableCell align="right" style={{fontStyle:'italic'}} dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.scientificName, searchQuery)}} />
                        <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.harvestType, searchQuery)}} />
                        <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.populationStatus, searchQuery)}} />
                        <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.calories, searchQuery)}} />
                      </TableRow>
                      
                    ))}
                    {/* padding for empty rows on the last page */}
                    {emptyRowsM > 0 && (
                      <TableRow
                        style={{
                          height: 53 * emptyRowsM,
                        }}
                      >
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </TableContainer>

              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 15, 20, 25]}
                  colSpan={3}
                  count={countM}
                  rowsPerPage={itemsPerPageM}
                  page={currentPageM}
                  SelectProps={{
                    inputProps: {
                      'aria-label': 'rows per page',
                    },
                    native: true,
                  }}
                  onPageChange={handleChangePageM}
                  onRowsPerPageChange={handleChangeItemsPerPageM}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
              </Grid>

              {/* INGREDIENTS */}
              <Grid item xs={4}>
                <TableContainer component={Paper}>
                <Table sx={Utils.TableStyle} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell><strong>{nameI}</strong></TableCell>
                      {attributesI.map((attribute) => 
                        <TableCell align="right"><strong>{attribute}</strong></TableCell>
                      )}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {currentItemsI
                      // .slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage)
                      .map((row) => (
                      <TableRow
                        key={row.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 }}}
                        onClick={ event => window.location.href=`/ingredients/${row.id}` } // very important for id to be specified
                        style={{cursor:'pointer'}}
                      >
                        <TableCell component="th" scope="row" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.ingName, searchQuery)}} />
                        <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.price, searchQuery)}} />
                        <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.consistency, searchQuery)}} />
                        <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.aisle, searchQuery)}} />
                        <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.percentFat, searchQuery)}} />
                      </TableRow>
                    ))}
                    {/* padding for empty rows on the last page */}
                    {emptyRowsI > 0 && (
                      <TableRow
                        style={{
                          height: 53 * emptyRowsI,
                        }}
                      >
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 15, 20, 25]}
                colSpan={3}
                count={countI}
                rowsPerPage={itemsPerPageI}
                page={currentPageI}
                SelectProps={{
                  inputProps: {
                    'aria-label': 'rows per page',
                  },
                  native: true,
                }}
                onPageChange={handleChangePageI}
                onRowsPerPageChange={handleChangeItemsPerPageI}
                ActionsComponent={TablePaginationActions}
              />
              </Grid>

              {/* RECIPES */}
              <Grid item xs={4}>
                <TableContainer component={Paper}>
                  <Table sx={Utils.TableStyle} aria-label="simple table">
                    <TableHead>
                      <TableRow>
                        <TableCell><strong>{nameR}</strong></TableCell>
                        {attributesR.map((attribute) => 
                          <TableCell align="right"><strong>{attribute}</strong></TableCell>
                        )}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {currentItemsR
                        .map((row) => (
                        <TableRow
                          key={row.name}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 }}}
                          onClick={ event => window.location.href=`/recipes/${row.id}` } // very important for id to be specified
                          style={{cursor:'pointer'}}
                        >
                          <TableCell component="th" scope="row" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.name, searchQuery)}} />
                          <TableCell align="right">
                            <img src={row.image} height='100vh' alt='Recipe Preview'/>
                          </TableCell>
                          <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.cost, searchQuery)}} />
                          <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.rating, searchQuery)}} />
                          <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.prepTime, searchQuery)}} />
                          <TableCell align="right" dangerouslySetInnerHTML={{__html: Utils.getHighlightedStringHTML(row.numServings, searchQuery)}} />
                      </TableRow>
                        
                      ))}
                      {/* padding for empty rows on the last page */}
                      {emptyRowsR > 0 && (
                        <TableRow
                          style={{
                            height: 53 * emptyRowsR,
                          }}
                        >
                          <TableCell colSpan={6} />
                        </TableRow>
                      )}
                    </TableBody>
                  </Table>
                </TableContainer>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 15, 20, 25]}
                    colSpan={3}
                    count={countR}
                    rowsPerPage={itemsPerPageR}
                    page={currentPageR}
                    SelectProps={{
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePageR}
                    onRowsPerPageChange={handleChangeItemsPerPageR}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </Grid>
            </Grid>


            </div>
        </div>
    );
}

export default GlobalSearch;