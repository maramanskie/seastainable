import { Box, Card, CardContent, Grid, Paper, Typography } from '@mui/material';
import NavBar from './NavBar'

/*
The public folder contains static assets which can be accessed online
Courtesy of https://wallup.net/ocean-tropical-fish-underwater/
*/
const bgPath = "/static/ocean.jpg"; 

// Landing/Splash page
function Home() {
    return (
        <div style={{ 
            backgroundImage: `url(${bgPath})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            width: '100vw',
            height: '100vh'
        }}>
            <NavBar></NavBar>
            <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            >
            <Card sx={{ 
                height: '75vh',
                width: '75%',
                mt: '5vh'
            }}>
            <CardContent>
                <Typography variant="h1" fontSize="3vw" display="flex"
                justifyContent="center"
                alignItems="center">
                🐟Welcome to Seastainable!💕
                </Typography>

                <Typography fontSize="1vw" display="flex"
                justifyContent="center"
                alignItems="center">
                Our mission aims to preserve ocean life and establish healthy relationships with planet earth.
                </Typography>

                <Typography fontSize="1vw" display="flex"
                justifyContent="center"
                alignItems="center">
                You've come to the right place if you want to discover new dishes while respecting marine ecosystems!
                </Typography>

                <Typography variant="h2" fontSize="3vw" display="flex"
                justifyContent="center"
                alignItems="center"
                mt='5vh'>
                Explore...
                </Typography>

                <Grid sx={{ flexGrow: 1, mt: '5vh' }} container spacing={2}>
                    <Grid item xs={12}>
                        <Grid container justifyContent="center" spacing={1}>

                            <Grid item>
                            <a href='/marinelife' style={{ textDecoration: 'none' }}>
                            <Paper
                                sx={{
                                height: '20vh',
                                width: '20vw',
                                objectFit: 'cover'
                                }}
                            >   
                                <Typography fontSize="2vw" display="flex"
                                justifyContent="center"
                                alignItems="center">
                                Marine Life 🐠
                                </Typography>
                                <img src={`https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F4animalmagnetism.com%2Fwp-content%2Fuploads%2F2019%2F03%2Ffish.jpeg&f=1&nofb=1`} alt='Fish Species' width='100%' height='100%'/>
                                
                                <Typography fontSize="1vw"
                                align="center">
                                    View information about different types of marine life used in various recipes
                                </Typography>
                                
                            </Paper>
                            </a>
                            </Grid>

                            <Grid item>
                            <a href='/ingredients' style={{ textDecoration: 'none' }}>
                            <Paper
                                sx={{
                                height: '20vh',
                                width: '20vw',
                                objectFit: 'cover'
                                }}
                            >   
                                <Typography fontSize="2vw" display="flex"
                                justifyContent="center"
                                alignItems="center">
                                Ingredients 🍅
                                </Typography>
                                <img src="https://locindustries.com/wp-content/uploads/2017/02/food-ingredients.jpg" alt='Recipe Ingredients' width='100%' height='100%'/>
                                
                                <Typography fontSize="1vw" 
                                align="center">
                                    Learn more about ingredients used in recipes based on their nutrition information and average price
                                </Typography>
                            </Paper>
                            </a>
                            </Grid>

                            <Grid item>
                            <a href='/recipes' style={{ textDecoration: 'none' }}>
                            <Paper
                                sx={{
                                height: '20vh',
                                width: '20vw',
                                objectFit: 'cover'
                                }}
                            >
                                <Typography fontSize="2vw" display="flex"
                                justifyContent="center"
                                alignItems="center">
                                Recipes 🍴
                                </Typography>
                                
                                <img src='https://eatwellenjoylife.com/wp-content/uploads/2016/12/Sauteed-Fish-Platter-6-copy-1024x664.jpg' alt='Fish Recipes' width='100%' height='100%'/>
                                <Typography fontSize="1vw" 
                                align="center">
                                    View recipes along with their cook time, ingredients and dietary information
                                </Typography>
                            </Paper>
                            </a>
                            </Grid>


                            
                        </Grid>
                    </Grid>
                </Grid>
                


            </CardContent>

            </Card>
            </Box>
        </div>
    );

}

export default Home;
