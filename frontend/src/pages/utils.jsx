export default class Utils {

  static checkOrEx(boolVal) {
    return boolVal ? '✔️' : '❌';
  }

  static replaceJSONSingleQuoteWithDoubleQuote(str) {
    // https://stackoverflow.com/questions/36038454/parsing-string-as-json-with-single-quotes COURTESY OF SEAN AH
    // {"key": value} is valid but we have {'key': value}
    const regex = /('(?=(,\s*')))|('(?=:))|((?<=([:,]\s*))')|((?<={)')|('(?=}))/g;
    str = str.replace(regex, '"');
    return str;
  }

  static replacePythonBoolWithJSBool(str) {
    // {"key": boolean} where boolean in (true, false) is valid but we have boolean in (True, False)
    str = str.replace(/False/g, 'false')
    str = str.replace(/True/g, 'true')
    return str;
  }

  static convertOurJSONStringToValidJSON(str) {
    // console.log("str: " + str);
    // str = Utils.replaceJSONSingleQuoteWithDoubleQuote(str);
    // console.log("str: " + str);
    str = Utils.replacePythonBoolWithJSBool(str);
    return JSON.parse(str);
  }

  static NUTRIENTS = {
    'FAT': 0,
    'CARBOHYDRATES': 1,
    'FLUORIDE': 2,
    'MANGANESE': 3,
    'PROTEIN': 4,
    'POTASSIUM': 5,
    'VITAMID D': 6,
    'COPPER': 7,
    'CALORIES': 8,
    'CAFFEINE': 9,
    'TRANS FAT': 10,
    'FOLATE': 11,
    'NET CARBOHYDRATES': 12,
    'SODIUM': 13
    // ADD MORE IF NECESSARY (only needed for pagination view)
  }

  static TableStyle = { minWidth: 650, "& .MuiTableRow-root:hover": { backgroundColor: "primary.light" }};

  static ENTER_KEYCODE = 13;
  static DEFAULT_ITEMS_PER_PAGE = 10;

  static getNutrientFromIngredientRecord(ingredient, nutrient) {
    let nutrition = Utils.convertOurJSONStringToValidJSON(ingredient.nutrition);
    let nutrients = nutrition.nutrients;
    return nutrients[nutrient];
  }

  static getNutrientInfoFromIngredientRecord(ingredient, nutrient) {
    let nutrientInfo = this.getNutrientFromIngredientRecord(ingredient, nutrient);
    return nutrientInfo['amount'] + " " + nutrientInfo['unit'];
  }

  // need to escape regex https://stackoverflow.com/questions/4029109/javascript-regex-how-to-put-a-variable-inside-a-regular-expression
  // https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
  static escapeRegexString(regexString) {
    return regexString.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }
  
  // get HTML wrapper to highlight the matching query -- regex capturing
  // use dangerouslySetInnerHTML to highlight (is there any way other than this though?)
  static getHighlightedStringHTML(text, query) {
    // query should get split up by spaces and each should get highlighted
    if (text) {
      query = query.toLowerCase();
      let matchList = query.split(" ");
      let escapedMatchList = matchList.map(Utils.escapeRegexString); // make sure user queries don't mess up the regex
  
      const regexString = "(" + escapedMatchList.join("|") + ")";
      // global match + ignore case sensitivity
      const regex = new RegExp(regexString, "gi");
      text = text.replace(regex, function(token) {
        if (token === '') return '';
        if (matchList.indexOf(token.toLowerCase()) !== -1) return "<mark>" + token + "</mark>";
        return token;
      });
      return text;
    }
    return "";
  }

  static getPercentage(percentage) {
    return parseFloat(percentage.substring(0, percentage.length - 1));
  };

  static getCaloricBreakdownData(ingredientData) {
    console.log(ingredientData);
    let ret = [];
    if (ingredientData) {
      let fat = Utils.getPercentage(ingredientData.percentFat);
      let carbs = Utils.getPercentage(ingredientData.percentCarbs);
      let protein = Utils.getPercentage(ingredientData.percentProtein);
      let rem = 100 - fat - carbs - protein;
      ret.push( {'name': 'Fat', 'value': fat} );
      ret.push( {'name': 'Carbs', 'value': carbs} );
      ret.push( {'name': 'Protein', 'value': protein} );
      if (rem > 0.01) {
        ret.push( {'name': 'Other', 'value': rem} );
      }  
    }
    return ret; 
  }
}
