import NavBar from './NavBar';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Grid from '@mui/material/Grid';
import { useState, useEffect } from 'react'
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import Utils from "./utils";
import { BACKEND_ENDPOINT } from './env';
import { LinearProgress } from '@mui/material';
import { PieChart, Pie, ResponsiveContainer, Tooltip } from 'recharts';

var cardStyle = {
  width: '900px', 
  height: '100%',
  marginTop: '20px'
}

var recipeImage = {
  height: 'auto',
  maxHeight: '350px',
  width: 'auto',
  maxWidth: '350px',
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto'
}

var cardLabels = {
  fontWeight: 'bold',
  fontSize: '17px',
  marginTop: '25px'

}

var dividerLabels = {
  fontWeight: 'bold',
  fontSize: '20px'
  
}

async function getIDForMarineLife(marineLifeName) {
  return axios.get(
    BACKEND_ENDPOINT + "/marinelife/name/" + encodeURIComponent(marineLifeName)
  ).then(response => {
    let id = (response.data && response.data[0] && response.data[0].id) ? response.data[0].id : -1;
    return id;
  })
}

async function getIDForRecipe(recipeName) {
  return axios.get(
    BACKEND_ENDPOINT + "/recipes/name/" + encodeURIComponent(recipeName)
  ).then(response => {
    let id = (response.data && response.data[0] && response.data[0].id) ? response.data[0].id : -1;
    return id;
  })
}

function IngredientInstance() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [data, setData] = useState({});
  const [marineLife, setMarineLife] = useState([]);
  const [recipes, setRecipes] = useState([]);

  useEffect(() => {
    const fetchIngredient = async() => {
      try {
        let response = await axios.get(
          BACKEND_ENDPOINT + "/ingredients/id/" + id
        );
        setData(response.data[0]);

        let ml = [];
        await Promise.all( 
          JSON.parse(response.data[0].fishType).map(async (mlName) => {
            let promisedID = await getIDForMarineLife(mlName);
            let id = Number(promisedID);
            ml.push({mlName, id});
          })
        );
        setMarineLife(ml);

        let recipes = [];
        await Promise.all( 
          JSON.parse(response.data[0].recipes).map(async (recipeName) => {
            let promisedID = await getIDForRecipe(recipeName);
            let id = Number(promisedID);
            recipes.push({recipeName, id});
          })
        );
        setRecipes(recipes);

      } catch (error) {
        console.error(error);
      }
    };

    fetchIngredient();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  
  return (
    <div>
    <NavBar></NavBar>

    <div style={{
      display: "flex",
      flexDirection: "column",
      alignItems: "center"
    }}>
      <Card sx={{ maxWidth: 900 }} style={cardStyle} >
        
        <CardContent>
        <Button variant="contained" color="primary" onClick={() => navigate(-1)}>
        <ArrowBackIcon></ArrowBackIcon>
        </Button>

        <CardMedia
          component="img"
          height="340"
          width="20"
          image={data.image}
          alt={`${data.name} recipe`}
          style={recipeImage}
        />

          <Typography gutterBottom variant="h4" component="div" style={{ textAlign: 'center', fontWeight: 'bold' }}>
            {data.name}
          </Typography>

          <Divider><div style={dividerLabels}>Ingredient Information</div></Divider>
          
          <Typography>
          { data.nutrition ?
            <div>
            <Grid container spacing={1}>
            <Grid item xs={6}>
              <div><strong>Name:</strong> {data.ingName}</div><br/>
              <div><strong>Aisle:</strong> {data.aisle}</div><br/>
              <div><strong>Consistency:</strong> {data.consistency}</div><br/>
              <div><strong>Unit Price:</strong> {data.price}</div><br/>
              <div><strong>Weight per Serving:</strong> {data.nutrition && 
                Utils.convertOurJSONStringToValidJSON(data.nutrition).weightPerServing.amount + 
                " " + 
                Utils.convertOurJSONStringToValidJSON(data.nutrition).weightPerServing.unit
              }</div>
            </Grid>
            <Grid item xs={6}>
            {data && 
              <Box style={{height: '200px', textAlign: 'center'}}>
                <div><strong>Caloric Breakdown by Percentage</strong></div>
              <ResponsiveContainer width="100%" height="100%">
                <PieChart width={400} height={400}>
                  <Pie dataKey="value" data={Utils.getCaloricBreakdownData(data)} cx="50%" cy="50%" innerRadius={40} outerRadius={80} fill="skyblue" />
                  <Tooltip />
                </PieChart>
              </ResponsiveContainer>
              </Box>
            }
            </Grid>
            </Grid>

            <div style={cardLabels}>Nutritional Information:</div>

            <Grid container spacing={1}>
            {Utils.convertOurJSONStringToValidJSON(data.nutrition).nutrients.map(nutrient => 
              <Grid item xs={6}>
                <strong>{nutrient.name}</strong>: {nutrient.amount} {nutrient.unit}
              </Grid>
            )}
            </Grid>
            </div>
            : <div>Loading information...<LinearProgress /></div>
          }

          </Typography>

          <Divider><div style={dividerLabels}>Marine Life that appears alongside this ingredient in Recipes</div></Divider>

          { data.fishType && marineLife !== [] ? 
              marineLife.map(item => 
                item.id !== -1 ? <span><Button href={`/marinelife/${item.id}`}>{item.mlName}</Button>, </span>  : item.mlName + ", "
              )
             : <div>Loading connections...<LinearProgress /></div>
          }

          <Divider><div style={dividerLabels}>Recipes this ingredient appears in</div></Divider>

          { data.recipes && recipes !== [] ? 
              recipes.map(item => 
                item.id !== -1 ? <span><Button href={`/recipes/${item.id}`}>{item.recipeName}</Button>, </span>  : item.recipeName + ", "
              )
             : <div>Loading connections...<LinearProgress /></div>
          }

        </CardContent>
      </Card>
    </div>
  </div>);
}

export default IngredientInstance;