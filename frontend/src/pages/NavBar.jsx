import { AppBar, Box, Link, Toolbar, MenuItem, Typography, Button, TextField } from '@mui/material';
import { useState } from 'react';
import Utils from "./utils";

const pages = ['Home', 'About', 'Marine Life', 'Ingredients', 'Recipes', 'Visualizations', 'Provider Visualizations'];

function NavBar() {
    const [searchBoxContents, setSearchBoxContents] = useState("");

    const monitorSearchBoxText = (event) => {
        setSearchBoxContents(event.target.value);
        let code = (event.keyCode ? event.keyCode : event.which);
        if(code === Utils.ENTER_KEYCODE) {
            handleSearch();
        }
    }

    const handleSearch = (event) => {
        window.location.href = `/global/${encodeURI(searchBoxContents)}`;
    };

      
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
            <Toolbar>
            {pages.map((page) => (
                <Link href={`/${page.replace(/\s/g, '').toLowerCase()}`} underline="none">
                <MenuItem key={page}>
                <Typography color='white' textAlign="center">
                    {page}
                </Typography>
                </MenuItem> 
                </Link>
            ))}
            <span style={{position: 'absolute', right: '10px', color: 'white'}}>
                <TextField size="small" id="outlined-basic" label="Global Search" variant="filled" onKeyUp={monitorSearchBoxText} style={{color: "white", marginTop: '10px'}} />
                <Button variant="contained" onClick={handleSearch} style={{marginRight:'10px', marginTop:'15px'}}>Go!</Button>
            </span>
            </Toolbar>
            </AppBar>
        </Box>
    );
}

export default NavBar;
