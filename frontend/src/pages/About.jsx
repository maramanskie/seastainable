import { useState, useEffect} from 'react';
import { Box, Typography } from '@mui/material';
import NavBar from './NavBar';
import caleb from './assets/images/caleb.jpg';
import mara from './assets/images/mara.jpg';
import carlos from './assets/images/carlos.jpg'
import geethika from './assets/images/Geethika.jpg';
import derrick from './assets/images/derrick.jpg';
import './App.css'
import Card from '@mui/material/Card';
import Divider from '@mui/material/Divider';


var cardText = {
    fontSize: '1vw'
}

var cardTextBold = {
    fontWeight: 'bold',
    fontSize: '2vw'  
}

var cardTextItalic = {
    fontStyle: 'italic',
    fontSize: '1vw'  
}

var largeText = {
    fontSize: '1.5vw',
    paddingLeft: '3vw',
    paddingRight: '3vw'
}

var cardPic = {
    height: '25vw',
    width: '25vw',
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
}

var centerItems = {
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    margin: '2vw'
}

var dividerLabels = {
    fontWeight: 'bold',
    fontSize: '20px'
    
  }

function Sources(props) {
    return(
        <a href={props.path} style={{
            color: "black",
            textDecoration: "none",
            width: "20vw"
        }} className = 'about-widget card'>
            <h4 style={{textAlign: "center"}}> {props.name} </h4>
            <img style={centerItems} src={props.image} alt={props.alternate} width='200vw' height='200vh'/>
            <br></br>
            <span style={{textAlign: "center", display: "block"}}>{props.description}</span>
        </a>
    );
}

function APIUsed(props) {
    return(
        <a href={props.path} style={{
            color: "black",
            textDecoration: "none",
            width: "20vw"
        }} className = 'about-widget card'>
            <h4 style={{textAlign: "center"}}> {props.name} </h4>
            <span style={{textAlign: "center", display: "block"}}>{props.description}</span>
        </a>
    );
}

function About() {
    const [isLoaded, setIsLoaded] = useState(false);
    const [commits, setCommits] = useState([]); 
    const [issues, setIssues] = useState([]);
    const [totalIssues, setTotalIssues] = useState([]);
    const [totalCommits, setTotalCommits] = useState([]);

    useEffect(() => {
        fetch('https://gitlab.com/api/v4/projects/33932416/repository/contributors')
          .then(res => res.json())
          .then(
            (result) => {
                var totalCommits = 0;
                var commits = {
                    "calebpark.edu@gmail.com": 0,
                    "mara.manskie@utexas.edu": 0,
                    "geethikah21@gmail.com" : 0,
                    "dliu0322@utexas.edu": 0,
                    "dliu0322@gmail.com": 0,
                    "carlosvela4567@gmail.com": 0
                };
                for (let i = 0; i < result.length; i++) {
                    commits[result[i].email] = result[i].commits;
                    totalCommits += result[i].commits;
                }
                setIsLoaded(true);
                setCommits(commits);
                setTotalCommits(totalCommits);
            });
    }, [])

    useEffect(() => {
        fetch('https://gitlab.com/api/v4/projects/33932416/issues?per_page=100')
          .then(res => res.json())
          .then(
            (result) => {
                var totalIssues = 0;
                var issues = {
                    "progammer": 0,
                    "maramanskie": 0,
                    "geethikah21" : 0,
                    "dliu0322": 0,
                    "carlosvela4567": 0
                };
                for (let i = 0; i < result.length; i++) {
                    issues[result[i].author.username] += 1;
                    totalIssues += 1;
                }
                setIsLoaded(true);
                setIssues(issues);
                setTotalIssues(totalIssues);
            });
    }, [])

    var unitTests = {
        "Caleb Park": 11,
        "Mara E Manskie": 19,
        "Geethika Hemkumar" : 10,
        "Derrick Liu": 0,
        "carlosvela4567": 12
    }


    return (
    <div>
        <NavBar></NavBar>

        <div style={centerItems}></div>

        <h1 class="section-title">About Us</h1>

        <Divider><div style={dividerLabels}></div></Divider>


        <Typography variant='h4' class="section-title">
            Purpose
        </Typography>

        <div style={largeText}><p>Seastainable is a website for people who are concerned with the environmental impact overfishing has on marine ecosystems. Users can search for sustainable fishing recipes based on a specific type 
            of fish or based on specific ingredients. They can then learn how to prepare corresponding dishes, see where the certain species of fish is from and make more sustainable decisions when consuming seafood.</p></div>
        
        <Typography variant='h4' class="section-title">
            Developer Team
        </Typography>


        <div className="cards-container">
        <AboutCards name={caleb} 
                role="Fullstack Engineer" 
                fullName="Caleb Park" 
                description="Hi! I'm a third year CS major at UT Austin. I'm from Plano, TX near Dallas. I love listening to EDM, eating good food and playing games with my friends. I also enjoy lifting weights and skiing."
                isLoaded={isLoaded}
                unitTests={unitTests["Caleb Park"]}
                commits={commits["calebpark.edu@gmail.com"]}
                issues ={issues["progammer"]}
                phase = "Phase 2 Leader">
        </AboutCards>

        <AboutCards name={mara} 
                role="Fullstack Engineer" 
                fullName="Mara Manskie" 
                description=" I’m a fourth year computer science major at UT Austin. I grew up just outside of Austin in the small town of Hutto, Texas. I like to crochet, go on walks or hikes and spend time with my cats."
                isLoaded={isLoaded}
                unitTests={unitTests["Mara E Manskie"]}
                commits={commits["mara.manskie@utexas.edu"]}
                issues ={issues["maramanskie"]}
                phase="Phase 1 Leader">
        </AboutCards>

        <AboutCards name={geethika} 
                role="Backend Engineer" 
                fullName="Geethika Hemkumar" 
                description="I’m a third year CS major at UT Austin. I’ve spent most of my life in Austin, TX. When I’m not working on school assignments/projects, I enjoy spending time reading, playing the piano, or playing card games such as Uno or Codenames. I’ve recently been playing a lot of Wordle."
                isLoaded={isLoaded}
                unitTests={unitTests["Geethika Hemkumar"]}
                commits={commits["geethikah21@gmail.com"]}
                issues ={issues["geethikah21"]}
                phase="Phase 3 Leader">
        </AboutCards>

        <AboutCards name={carlos} 
                role="Backend Engineer" 
                fullName="Carlos Vela" 
                description="I'm a fourth year (senior) CS major at the University of Texas at Austin. I was born and raised in Laredo, Texas and move to Austin to attend the university. In my spare time, I enjoy going to the gym to lift, watching sporting events, and playing video games with my friends from back home."
                isLoaded={isLoaded}
                unitTests={unitTests["carlosvela4567"]}
                commits={commits["carlosvela4567@gmail.com"]}
                issues ={issues["carlosvela4567"]}
                phase="Phase 4 Leader">
        </AboutCards>

        <AboutCards name={derrick} 
                role="Frontend Engineer" 
                fullName="Derrick Liu" 
                description="  I’m a fourth year computer science major at UT Austin. I grew up in the Woodlands, Texas. I like to cook, complain about working out, and play games with friends."
                isLoaded={isLoaded}
                unitTests={unitTests["Derrick Liu"]}
                commits={commits["dliu0322@gmail.com"]+commits["dliu0322@utexas.edu"]}
                issues ={issues["dliu0322"]}>
        </AboutCards>
        </div>

        <Typography variant='h4' class="section-title">
            GitLab Stats
        </Typography>

        <div className="sources-container">
            <GitLabStats  classname="large-text" totalCommits={totalCommits}
                    totalIssues={totalIssues}
                    totalUnitTests={52}>
            </GitLabStats>
        </div>

        <Typography variant='h4' class="section-title">
            APIs Used
        </Typography>

        <div className="sources-container">
        <APIUsed name="FishWatch API" 
                description="Can be scraped by an input parameter of the type of fish (species), and we will use the fields in the returned object to gather info about a fish" 
                path="https://www.fishwatch.gov/">
        </APIUsed>


        <APIUsed name="Recipe API" 
                description="Can be queried for different recipes which may potentially include certain fish as ingredients" 
                path="https://developer.edamam.com/edamam-recipe-api">
        </APIUsed>

        <APIUsed name="Ingredients API" 
                description="Can be queries to get nutrition information, cost, and other fields related to the ingredients of a specific recipe" 
                path="https://spoonacular.com/food-api/docs#Ingredient-Search">
        </APIUsed>

        <APIUsed name="GitLab API" 
                description="Exposes data about project commits + issues, which we can filter for each developer id and pull their stats" 
                path="https://gitlab.com/api/v4/">
        </APIUsed>
        </div>

        <Typography variant='h4' class="section-title">
            Tools Used
        </Typography>

        <div className="sources-container">
        <Sources name="React" 
                description="We used React, a javascript library to create components to render and interact with the DOM. Create React App was used to bootstrap this project."
                path="https://reactjs.org/" 
                image="https://coder.clothing/images/stories/virtuemart/product/react-logo.png" alternate="React Logo">
        </Sources>

        <Sources name="Postman" 
                description="Postman was instrumental in making HTTP REST calls to APIs and setting up API documentation." 
                path="https://www.postman.com/" 
                image="https://yt3.ggpht.com/a/AGF-l791ySSDFwSHTYVjI0BMuuyqlFmiMutGcvcYcA=s900-c-k-c0xffffffff-no-rj-mo" alternate="Postman Logo">
        </Sources>

        <Sources name="Material UI" 
                description="Material UI provided a suite of styled components that we could use to build our frontend." 
                path="https://mui.com/" 
                image="https://vscodeshift.gallerycdn.vsassets.io/extensions/vscodeshift/material-ui-snippets/3.3.0/1591764008680/Microsoft.VisualStudio.Services.Icons.Default" alternate="Material UI Logo">
        </Sources>

        <Sources name="GitLab" 
                description="GitLab housed our git repositories, managed our version control + issues, and integrated with our CI/CD pipelines." 
                path="https://about.gitlab.com/" 
                image="https://www.gillware.com/wp-content/uploads/2017/02/gitlab-logo-square.png" alternate="GitLab Logo">
        </Sources>

        <Sources name="AWS Amplify" 
                description="AWS Amplify was used to deploy our webapp." 
                path="https://aws.amazon.com/amplify/" 
                image="https://backendless.com/wp-content/uploads/2020/02/AWS-Amplify-Logo-Small.png" alternate="AWS Amplify Logo">
        </Sources>


        <Sources name="Node.js" 
                description="NodeJS and npm served as the underlying runtime and package manager for our webapp." 
                path="https://nodejs.org/en/" 
                image="https://1.bp.blogspot.com/-mzw13XQJPYM/XgzNHXSUdXI/AAAAAAAAAYY/xeIhLBEpTQUn8huUCnWXdUX6vIR_T4UCQCPcBGAYYCw/s1600/http___pluspng.com_img-png_nodejs-png-nodejs-icon-png-50-px-1600.png" alternate="Node.js Logo">
        </Sources>

        <Sources name="NameCheap" 
                description="Namecheap was used to claim our domain name." 
                path="https://www.namecheap.com/" 
                image={`https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.pnglib.com%2Fwp-content%2Fuploads%2F2020%2F08%2Fnamecheap-logo_5f3e4f465e893.png&f=1&nofb=1`} alternate="NameCheap Logo">
        </Sources>

        <Sources name="Docker" 
                description="Specified images for building containers to develop and deploy our applications within" 
                path="https://www.docker.com/" 
                image={`https://www.docker.com/wp-content/uploads/2022/03/vertical-logo-monochromatic.png`} alternate="Docker Logo">
        </Sources>


        <Sources name="Selenium" 
                description="Selenium was used to write acceptance tests for our GUI" 
                path="https://www.selenium.dev/" 
                image={`https://upload.wikimedia.org/wikipedia/commons/d/d5/Selenium_Logo.png`} alternate="Selenium Logo">
        </Sources>

        <Sources name="Flask" 
                description="Flask was used with our backend and database" 
                path="https://flask.palletsprojects.com/en/2.1.x/" 
                image={`data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAh1BMVEX///8AAAD7+/v29vbw8PDv7+/c3Nzq6ur39/fAwMDJycnz8/PCwsK4uLiDg4Pj4+PPz8+Li4ttbW3W1tZ9fX13d3cQEBDd3d03Nzc8PDylpaVVVVWWlpatra1ERERnZ2dNTU0fHx+cnJwsLCwVFRUtLS0kJCRgYGBBQUFTU1OIiIiRkZEaGhoW0LxYAAAPeElEQVR4nO1da3uiOhAGrSAoooA3FO/VtvT//77DJOE+CbhHZXYf3w/ntGrZjJnMfSaa9sYbb7zxxhtvvPHGXwzXGiew+l2v42mY6gJ21yt5EoyEtnjpnv5dEn8FZVZCYtdreQocXd/wn1a67nS7ludgltDl+tOxZR10fdL1ap6BUC9g3/VqnoFBkcKfrlfzFEwKFI67XsxzsM4pHHW9ludgeMwo7Hopz8JoIQj87HolT4Ob7mGv65U8DanKWDmu2WPoekWPhqvXsA67XtRDEdUp/LeU4xQj8F+SrBZO4L8jd2IJgbrb9coeg9FORuA/soeSI8hw6npxj8BYQWDiGptdr+//46qkUNe9adcr/J/4wIi6rUq/f3rh9C/2OY51ChNbZmTvKy8e99FfKnfCOoUr9sZoXnvD73itfwaMTQ3+1rr6+qHLhf45EDblwcVR/Y2o47X+GW7IJjJKMD1idb3aP8EnQogej8oBqgzeoOv13g0ToyPB2Us15aFCo9H1ku8ExqQ5rpNxX1tVXjz8VUaAJ6fu4jmcJetq4/uv0RvOUUbeJlxmn0JUpr75C2jsO7L928VlNsS9jwPxXKNbtcpyDhTkzT6F/zuR+Y+/J8JpcYzxCkTG4w/Qhh/wUaV75VFlVltJYIYr2NqB+jMrmgb5dzsK9V3y2cpLwTZypq7J+LPnMqtgTnAjqxpOCi9KP/rpnb7g/xVHkbP7J7mN3LSlkGEVOO5QEzHHSqJ/mO0trXiHLD5ax3pm59sDGap5+UkFo+9GypyruX4ogvGw9FcgoC7lB5VCrTtCrseskbpDaGq9peWM7YIcCS7rVcm58Kt/RSSeY/02kOdBiVsoNtqTPwjJWFEIk/fU7sQ6Zpt2Yr8sdp6idgE157p3II0Ftq5084Q7odnR2F8O1DrghD7i6wU0KCE3wvbRsvnPczjMQz5MZl5Fu3ZcOIYSuDjMrPvU2VTYclxBDJwi41+fse7WMDACj/c9Y+TMU0bPtUPfyZ2VLu2bHs6fcesH9O24yJPlN6cx+Fn7jwcv+i4UmWkx8zOCA/72+HyVp+5NP/Qqcbl17UPDQccKMV/chEmVSWmpffixIgr7A2PsRDPvXNl2JmY2LyegEWJ5mXWVliXymkuuv7OsqBHNV6hpsDn5Pa3/TZhCRzO9W8DCLGmRiZCkTMMx18//QU/sbuKkRsuALoUudwj4vglCvsUHBnNd3ya6DnEgV0FUdh7OyDnsHHwHucARipnbqLvc1uprEcqbNRFyo0phmlDLVmzY06IGi44of4qsYgFzohQeWXG+XtJlyyhKHcETYrXaUQCKrqpIAoq1mpwyHgnO7JEx2zRwKFw03cS9+qUTVByj4F5j6BWABRtpHErYHmlW4rDHHP/FtzQrOtEXr1p3e/AtSVfPPL962iVHIH0QkB0IxUIKbN1ORsHR+1GW00j9jQDUS5CxASGoyKlDyp/ggh1HnuIr6Ax3xUnlniw7sJcVxfITJHIkwWIij7fkTyHYf6KSKynmoe0qndg8nkyw001Wl1BA80NyUUWx1l2aGL2DwjzWM3v+gu+GupwU0Cw98ma3eeNnX4+hgjaOW/NDQCRPxu0++3qoA96A5rj8khXxbYi2SaGtIyVkeQp5YVAEEUNQGi9Z8p3Aqi0rSFWFojCIGaRfNCnUvhopTPduwSIacmwopGEQ4PkUjMJr4WcMCYV3ZTpeBTSwj3LpVldLy436C+gODfRd8lVDClvxoBtJs01T2qb7yEojhgPbNrThSTUpY0O1qcaR0bfINeGYRzQmI02Rhzjck9N5JZAKdY5cMDLL5wY5QpUwPStz/F1CUhFVdPZY0O2kjT1VtEkn25+AxgwrUpPlmpId6smdXMjN1cLENIBWRC3KxUFDVr++U3nCLPtIrZ6NY4BRmMn9pZA301tZ+NRx0SnGExkuCIVZYK2nb0Q4vGfHC9U0F6jhJFXLlgPTiKKCIvoxknfXGfOpuDDUScaiAKgHtbZtUPc8Vd+qE3+ok+2EUgQVI+FdtXrOmarKV5qmN17128ot+qYZqQHIYt+Q/N2x5gP5+St4xReyKl/SjHAeMZt1sVNZY6M8WQHVKRfpB7vFEqUQdB8UVJq+Qn6csqoG7mm+YLV/BIxAli2EoHhBA5j6LSor9XU+etBtfWA7gGh52hYpZNwHbFrYQVYmVdxRMNWyAgWopSaq8hkhky/dLhjhXOXDC9CtnsoTVrhQMMrZvqWbCFXeNL18zoyboR4UixUZHR7bMyfTAjyAnBc/GUWKQ51mcoYBfHjD2JVC4McBf90a5Sl8IXUz2crYVogX1olAVSGymCJsRTmkMWLesW/nFKbmT3YWWeKf8TAnftfF6tvAFeqhbMB5LII4clIKzTwgkLpRTDixw5cc0b1OV11oR85h/RKFC0aRlu5hrC8zK/0srJxMuiYG7NTWKVYrCLCumQ8hLLOTmLwRx0BVQuFgx15IP5AKUEf87CZWNwgqov6T6Ab5qXjDXGYuD0ChJfhWxDx+s7+c8Toa45D8Z0czD8zB9qZXyZgmfOswDzFINWUkxG3B4J4waqdg65zo2t6CTU9apRZxo31UXvHBxPsuxWvyXwzKooZb37XQ4rwaMT5rS0cuTXS6dpuIV0S1FoxxtSRFaVvvCVs1PJF4rrUinqtKUvmQiGi5AgPfqnEtjTHN/Mfbfq1f1NrApHwQNRbWXmuV6SxM2fPe0HOLiPaV8qh6run8WmwR3hP9vc2NvRPSU845K1Z7ecFKyahuTIHad/e+vRJcyNQypsdpYVJEk4cLJgNdfcFlTfUcFgBRx6ZmtANlw60mZDgW87RqH05q0yk70Q0parIZJ4m68Hnyxgad2SBQDZ1q0QlDta2Q8ya8MwwTT9EEi6dJGeiEQxmyOj7hR8BM71mzOPWa97lLoBQWsvMjVV8JB8hiqjFFTYRdcD7lmDX2UZo62YoFgKwYM0z5bpRFYkZbyYk86xQ7LzJIx0DN+ukHFpGdeIhgoSeO0rZeuwB9qERLhwCKilreVCIqN3hnm61t6sE1pnPIhtzUs7yCQfUDvUSuDCtPYE50kzzqEGh1TQZvWR7EsOvVxUqPxeuIzN7B0DCQ7ViepODU+/B5lwrhixRa1LYXAZ5jydROrYYq8xICXsknA6uEL6WJBQh7GHDQjs3l3yUU/nyaBlgJi1PwIOrBjBQ/2AzCUnhD5Mwp32i2gCS3xBOONa0/rlXClYOkosOBsGEDxnOMN32ljlGlTaPCkVwek82Wajz+bWIl7lnxTLl0umah8pcJuxgQb1tjfW1pSL9YV/w7qSuGWLfAACfsJ/IajDqFqftb6NtHPSX3xnIBhJ19dgj7SIcp35WSoEVlZo+fZsIRG6isuCHjvbkeL88PlPnEgU5xVEYGECUWwqfcHyxnqH4lW/VFmk9ZcB/pVBAF4JU2lA1KI5i4ZCsXeH37HGlP5DxZq5y+YJlRcIaJNihowpGdIlFi7hfVq24vSL5iRlrvgyw8Yr1t/CgiVh1Sabsi7SmyPnukHUNcrVOnECFxQPoosoCGgcyPvDGtiFl19QCjpdMYtYsDztonWujO4m7IUA1E/3m1NjFKgE6EGI/1zwdaDxl1Vtca8KnOR+1KwUISrmQY9qk23FpHhQ0YQHS9YWDQlaxtaIW0hGGaHwI/dMsXvhmfNg9eEMD7nq461TZvTegEt3k+CIfE5XUl/EsDFuPTdgR+yyw0ZhgRmrJfBhy2WHV3ZxEbScbNUexw94AONl86l6CKK86Nc8q7yE6R5pb9pa3jhBNJJ/8WC8/olHcR5Mx3JYDIpb+JzTdNENdp5ExAVdxAMOOnMhgsnR9h4bd/1L1F/jmqSgPip+PKbIJ5uk8+etnXsepRiJNMNKzBvAy3WqoRM5e374dYQ3/igVR0h3iZ4ExsAFigi142DbuwVShxAmXBkmbtzjS7MMFAhXhE8xjCIkosmXcc0YyipjUkrU1UhmtxLG3uitCUN5DFmGnaED90UhTDU7lldCOZ0QB3OGw1EbSEIksa+MtkAE6QU7/U6R4SB3l9J8U8fx8YNJJdYilHKRecF7N8EpxfNxIk3ruLpQxxwTIiOCdkBGYonMWmW77KKF8X9JFrnDO90zgC0uAEtZmxnKPSC+XnKscjVx3GSAS96Cu6Fuqo+hRW7niR0409WNsBfjIbKuAyrAPk+q+CvX6v19h79r7D5p2ZVd3uhlYvtA1s4v4ou/Vqdcdx7MPtoIsnbzw7g0w+tryEFoDPGB6Lndy1bXhL3fAniyjmRDFRr7jHuwKZ0zQQNS2rNkGcYVZx9ux4CHeF55bbPA88hZyv0mDstfEGWjePmTy9LBCfSqSC4kvP483Vm13KKNhSL6iUH93nRambiQoxroMj3cjioX9Juu6+Qlt1i3cpOrLHv4yiNfwiU6j9LcIMStlQeda8nhgvngvl0O1Hwmx3jXCGlfcjFZijqlT+DstzGwpvvTKWFev34rcsS/pudjtI/Xap4z5MIwTFpMLipfEB/z4nA8CFRG9pZup+HwnZv4xOQTXfvDk5zqwUOXl13UP+zS/aRXBYwhtES/HLWYeZXWcob2RKFIWP2oBP3Fgj6zA5nFqpkMC0sV7VjZNd9tbiIdevze0nsjPFn3xl5423jSzDHXwAHno/aL6icNrejMuwi1M+uFliJ8btH+NtLds+YV/Z7wOTeR/Zgi6RiTdpSgGjbgpJ9H2qJ4ywVoStun0axSPnVxS89rBnSfKKGFhUvLxj+3QntWkmdmKfJ0EGvrOtXhotx2OLBp1czkSa33y5iwA7LTUeu3HbrZdRGFpT33Czo9WTDpIv46EUJjRmXLQeaB9tVeX1x0bLdXZBgDXn6JfVwZtP2gijx1NYPE+gqbH7WV+Mh1OY0JgyHBPjeO70dXjOuCObzR3WP7mobs2sT8Gz3OSlUG8x/wecez3JB2ETPtHM6UV8Iw/c4DRezqyH0/P75tyYm5fr7TSR8UNJVcozsJjYr3I+jJmwoS/z0DaiO53JP8N6++KElhvusn/8fj+rHY5eMJmzmeuHsJPG3N549kS9uLFSh8Q1ukyc9+1tfbD9V2zZU9/3p2PLCuNgfmjwC+v4DGnVrZh2OD+UBc7amyQO3SD/8vum608dy4niyU7N1ee9Yj5lpxj61jaoEKpfPjfez+wUOY4Thad4fkCnOaW0JZ7vlFzWsYaeOXXCyW3XWol8Mgfed+mTVsPAt6Pt5La+IsQujp+HII5sg25Hyl3oD0xzmcie8ThxBl3THJGsKnrjjTfeeOONN9544+H4D0pXwVwunEBsAAAAAElFTkSuQmCC`} alternate="Flask Logo">
        </Sources>

        <Sources name="Postgres" 
                description="Postgres served as our foundation for our database" 
                path="https://www.postgresql.org/" 
                image={`https://upload.wikimedia.org/wikipedia/commons/2/29/Postgresql_elephant.svg`} alternate="Postgres Logo">
        </Sources>

        <Sources name="Elastic Beanstalk" 
                description="Elastic Beanstalk was used to deploy our backend" 
                path="https://aws.amazon.com/elasticbeanstalk/" 
                image={`https://d1.awsstatic.com/icons/console_elasticbeanstalk_icon.0f7eb0140e1ef6c718d3f806beb7183d06756901.png`} alternate="Elastic Beanstalk Logo">
        </Sources>

        {/* <Sources name="" 
                description="" 
                path="" 
                image={``} alternate="">
        </Sources> */}

        </div>

        <Typography variant='h4' class="section-title">
            Links to View Our...
        </Typography>

        <div className="sources-container">
        <APIUsed style={largeText} name="API Documentation" 
                path="https://documenter.getpostman.com/view/20307881/Uyr4Kfk4">
        </APIUsed>

        <APIUsed style={largeText} name="GitLab Repo" 
                path="https://gitlab.com/maramanskie/seastainable">
        </APIUsed>
        </div>
    </div>
    );
}

function AboutCards(props){
    return(
        <Card className="card-spacing" >
            <figure className="figure" style = {centerItems}>
            <img style={cardPic} src={props.name} className="figure-img img-fluid z-depth-1"
            alt="" />
                <figcaption className="figure-caption" style={cardText}>
                    <div style={cardTextItalic}>{props.role}</div>
                    <br></br>
                    <div style={cardTextBold}>{props.fullName}</div><br/>
                    <span style={{centerItems}}>
                        <Box sx = {{width: "25vw"}} style = {{centerItems}}>
                            {props.description}
                        </Box>
                    </span><br/>
                    <h4>{props.phase}</h4>
                    <br></br>
                    Commits: {props.isLoaded ? props.commits : "Loading..."}
                    <br></br>
                    Issues: {props.isLoaded ? props.issues : "Loading..."}
                    <br></br>
                    UnitTests: {props.isLoaded ? props.unitTests : "Loading..."}
                </figcaption>
            </figure>
        </Card>
    );
}

function GitLabStats(props){
    return(
        <div className = 'gitlab-widget card'><p>Total Commits: {props.totalCommits}<br/>   Total Issues: {props.totalIssues}<br/>     Total Unit Tests: {props.totalUnitTests}</p></div>  
    );
}


export default About;