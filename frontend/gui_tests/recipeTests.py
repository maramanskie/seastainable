from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
import time


class TestRecipes(unittest.TestCase):
    def setUp(cls):
        ops = Options()
        ops.add_argument("--headless")
        ops.add_argument("--window-size=1280,800")
        ops.add_argument("--no-sandbox")
        ops.add_argument("--disable-dev-shm-usage")

        s = ChromeService(ChromeDriverManager().install())

        cls.driver = webdriver.Chrome(service=s, options=ops)
        cls.link = "https://www.seastainable.me/recipes"

        cls.driver.get(cls.link)

    def tearDown(cls):
        cls.driver.quit()

    # test sorting by recipe name asc
    def testSort(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/span[2]/div[1]/div")
                )
            )
            a.click()

        except Exception as ex:
            print("Couldn't open Sort On dropdown: " + str(ex))
            return

        time.sleep(2)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div[2]/div[3]/ul/li[2]")
                )
            )

            a.click()
            self.assertEqual(a.text, "Recipe Name")

        except Exception as ex:
            print("Couldn't find Recipe Name option in dropdown: " + str(ex))
            return

        time.sleep(4)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/div/div/table/tbody/tr[1]")
                )
            )

            self.assertEqual(
                a.text,
                "Acadia's Sauteed Italian Flounder $2.76 49/100 11 minutes 2 servings",
            )
            a.click()

        except Exception as ex:
            print(
                "Couldn't find Acadia's Sauteed Italian Flounder recipe in Sort"
                + str(ex)
            )
            return

    # test filtering by prep time less than an hour
    def testFilter(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/span[2]/div[3]/div/div")
                )
            )
            a.click()

        except Exception as ex:
            print("Couldn't open Filters dropdown: " + str(ex))
            return

        time.sleep(2)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div[2]/div[3]/ul/li[6]")
                )
            )

            a.click()
            self.assertEqual(a.text, "Preparation Time is under an hour")

        except Exception as ex:
            print(
                "Couldn't find Preparation Time is under an hour option in dropdown: "
                + str(ex)
            )
            return

        time.sleep(4)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/div/div/table/tbody/tr[1]")
                )
            )
            self.assertEqual(
                a.text,
                "Celery, Orange and Smoked Mackerel Salad $2.36 81/100 45 minutes 1 servings",
            )

        except Exception as ex:
            print(
                "Couldn't find Celery, Orange and Smoked Mackerel Salad recipe in Filter"
                + str(ex)
            )
            return

    # test searching for swordfish recipes
    def testSearch(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/span[1]/div/div/input")
                )
            )

            a.send_keys("swordfish")
            a.send_keys(Keys.ENTER)

        except Exception as ex:
            print("Couldn't Search for swordfish " + str(ex))
            return

        time.sleep(4)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/div/div/table/tbody/tr[1]")
                )
            )

            self.assertEqual(
                a.text,
                "Pan Roasted Swordfish Steaks $4.82 80/100 25 minutes 2 servings",
            )

        except Exception as ex:
            print(
                "Couldn't find Pan Roasted Swordfish Steaks recipe in Search" + str(ex)
            )
            return


if __name__ == "__main__":
    unittest.main()
