from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
import time


class TestMarineLife(unittest.TestCase):
    def setUp(cls):
        ops = Options()
        ops.add_argument("--headless")
        ops.add_argument("--window-size=1280,800")
        ops.add_argument("--no-sandbox")
        ops.add_argument("--disable-dev-shm-usage")

        s = ChromeService(ChromeDriverManager().install())

        cls.driver = webdriver.Chrome(service=s, options=ops)
        cls.link = "https://www.seastainable.me/marinelife"

        cls.driver.get(cls.link)

    def tearDown(cls):
        cls.driver.quit()

    # test sorting by harvest type asc
    def testSort(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/span[2]/div[1]/div/div")
                )
            )
            a.click()

        except Exception as ex:
            print("Couldn't open Sort On dropdown: " + str(ex))
            return

        time.sleep(2)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div[2]/div[3]/ul/li[4]")
                )
            )

            a.click()
            self.assertEqual(a.text, "Harvest Type")

        except Exception as ex:
            print("Couldn't find Harvest Type option in dropdown: " + str(ex))
            return

        time.sleep(4)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/div/div/table/tbody/tr[1]")
                )
            )

            self.assertEqual(
                a.text,
                "Sugar Kelp Saccharina latissima Farmed The population level is unknown. 43 calories",
            )

        except Exception as ex:
            print("Couldn't find Sugar Kelp marine life on Sort" + str(ex))
            return

    # test filtering by farmed harvest type
    def testFilter(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/span[2]/div[3]/div/div")
                )
            )
            a.click()

        except Exception as ex:
            print("Couldn't open Filters dropdown: " + str(ex))
            return

        time.sleep(2)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div[2]/div[3]/ul/li[2]")
                )
            )

            a.click()
            self.assertEqual(a.text, "Harvest Type is Farmed")

        except Exception as ex:
            print("Couldn't find Harvest Type is Farmed option in dropdown: " + str(ex))
            return

        time.sleep(4)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/div/div/table/tbody/tr[1]")
                )
            )

            self.assertEqual(
                a.text,
                "Eastern Oyster Crassostrea virginica Farmed The population level is unknown. 81 calories",
            )

        except Exception as ex:
            print("Couldn't find Eastern Oyster marine life on Filter" + str(ex))
            return

    # test searching for swordfish marine life
    def testSearch(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/span[1]/div/div/input")
                )
            )

            a.send_keys("swordfish")
            a.send_keys(Keys.ENTER)

        except Exception as ex:
            print("Couldn't Search for swordfish " + str(ex))
            return

        time.sleep(4)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/div/div/table/tbody/tr[1]")
                )
            )

            self.assertEqual(
                a.text,
                "North Pacific Swordfish Xiphias gladius Wild Above target population levels. 121 calories",
            )

        except Exception as ex:
            print(
                "Couldn't find North Pacific Swordfish marine life on Search" + str(ex)
            )
            return


if __name__ == "__main__":
    unittest.main()
