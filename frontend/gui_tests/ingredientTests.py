from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
import time


class TestIngredients(unittest.TestCase):
    def setUp(cls):
        ops = Options()
        ops.add_argument("--headless")
        ops.add_argument("--window-size=1280,800")
        ops.add_argument("--no-sandbox")
        ops.add_argument("--disable-dev-shm-usage")

        s = ChromeService(ChromeDriverManager().install())

        cls.driver = webdriver.Chrome(service=s, options=ops)
        cls.link = "https://www.seastainable.me/ingredients"

        cls.driver.get(cls.link)

    def tearDown(cls):
        cls.driver.quit()

    # test sorting by ingredient name asc
    def testSort(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/span[2]/div[1]/div/div")
                )
            )
            a.click()

        except Exception as ex:
            print("Couldn't open Sort On dropdown: " + str(ex))
            return

        time.sleep(2)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div[2]/div[3]/ul/li[2]")
                )
            )

            a.click()
            self.assertEqual(a.text, "Ingredient Name")

        except Exception as ex:
            print("Couldn't find Ingredient Name option in dropdown: " + str(ex))
            return

        time.sleep(4)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/div/div/table/tbody/tr[1]")
                )
            )

            self.assertEqual(
                a.text, "agave syrup $0.01 liquid Ethnic Foods;Health Foods 1.31%"
            )

        except Exception as ex:
            print("Couldn't find agave syrup ingredient on Sort" + str(ex))
            return

    # test filtering by price per serve less than $1
    def testFilter(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/span[2]/div[3]/div/div")
                )
            )
            a.click()

        except Exception as ex:
            print("Couldn't open Filters dropdown: " + str(ex))
            return

        time.sleep(2)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div[2]/div[3]/ul/li[8]")
                )
            )

            a.click()

            self.assertEqual(a.text, "Price per Serving <= $1.00")

        except Exception as ex:
            print(
                "Couldn't find Price per Serving <= $1.00 option in dropdown: "
                + str(ex)
            )
            return

        time.sleep(4)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/div/div/table/tbody/tr[1]")
                )
            )

            self.assertEqual(
                a.text, "dillweed $0.04 solid Produce;Spices and Seasonings 19.38%"
            )

        except Exception as ex:
            print("Couldn't find dillweed ingredient on Filter" + str(ex))
            return

    # test searching for butter and peanut ingredients
    def testSearch(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/span[1]/div/div/input")
                )
            )

            a.send_keys("butter peanut")
            a.send_keys(Keys.ENTER)

        except Exception as ex:
            print("Couldn't Search for butter and peanut" + str(ex))
            return

        time.sleep(4)

        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, "/html/body/div/div/div[2]/div/div/table/tbody/tr[1]")
                )
            )

            self.assertEqual(
                a.text, "unsalted butter $0.01 solid Milk, Eggs, Other Dairy 99.5%"
            )

        except Exception as ex:
            print("Couldn't find unsalted butter ingredient on Search" + str(ex))
            return


if __name__ == "__main__":
    unittest.main()
