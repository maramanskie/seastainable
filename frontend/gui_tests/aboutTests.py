from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


class TestAbout(unittest.TestCase):
    def setUp(cls):
        ops = Options()
        ops.add_argument("--headless")
        ops.add_argument("--window-size=1280,800")
        ops.add_argument("--no-sandbox")
        ops.add_argument("--disable-dev-shm-usage")

        s = ChromeService(ChromeDriverManager().install())

        cls.driver = webdriver.Chrome(service=s, options=ops)
        cls.link = "https://www.seastainable.me/about"

        cls.driver.get(cls.link)

    def tearDown(cls):
        cls.driver.quit()

    # test it goes to the correct page
    def testAbout(self):
        self.driver.get(self.link)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "About Us")

    # test stats are present
    def testStats(self):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "gitlab-widget"))
            )
        except Exception as e:
            print(e)
            return

    def testAboutNav(self):
        self.driver.get(self.link)
        self.driver.find_element(By.LINK_TEXT, "Marine Life").click()
        time.sleep(2)
        self.assertEqual(
            self.driver.find_element(By.TAG_NAME, "strong").text,
            "Viewing All Marine Life 🐟",
        )
        self.driver.back()
        time.sleep(2)
        self.driver.find_element(By.LINK_TEXT, "Ingredients").click()
        time.sleep(2)
        self.assertEqual(
            self.driver.find_element(By.TAG_NAME, "strong").text,
            "Viewing All Ingredients 🍅",
        )
        self.driver.back()
        time.sleep(2)
        self.driver.find_element(By.LINK_TEXT, "Recipes").click()
        time.sleep(2)
        self.assertEqual(
            self.driver.find_element(By.TAG_NAME, "strong").text,
            "Viewing All Recipes 🍴",
        )
        self.driver.back()
        time.sleep(2)
        self.driver.find_element(By.LINK_TEXT, "About").click()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "About Us")
        self.driver.back()


if __name__ == "__main__":
    unittest.main()
