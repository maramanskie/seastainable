from selenium import webdriver
import unittest
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager


class TestNavBar(unittest.TestCase):
    def setUp(cls):
        ops = Options()
        ops.add_argument("--headless")
        ops.add_argument("--window-size=1280,800")
        ops.add_argument("--no-sandbox")
        ops.add_argument("--disable-dev-shm-usage")

        s = ChromeService(ChromeDriverManager().install())

        cls.driver = webdriver.Chrome(service=s, options=ops)
        cls.link = "https://www.seastainable.me/"

        cls.driver.get(cls.link)

    def tearDown(cls):
        cls.driver.quit()

    def testHome(self):
        self.driver.get(self.link)
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Marine Life").text, "Marine Life"
        )
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Recipes").text, "Recipes"
        )
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Ingredients").text, "Ingredients"
        )

    def testMarineLife(self):
        self.driver.find_element(By.LINK_TEXT, "Marine Life").click()
        element = self.driver.find_element(By.TAG_NAME, "strong")
        self.assertEqual(element.text, "Viewing All Marine Life 🐟")
        currURL = self.driver.current_url
        self.assertEqual(currURL, self.link + "marinelife")

    def testIngredients(self):
        self.driver.find_element(By.LINK_TEXT, "Ingredients").click()
        element = self.driver.find_element(By.TAG_NAME, "strong")
        self.assertEqual(element.text, "Viewing All Ingredients 🍅")
        currURL = self.driver.current_url
        self.assertEqual(currURL, self.link + "ingredients")

    def testRecipes(self):
        self.driver.find_element(By.LINK_TEXT, "Recipes").click()
        element = self.driver.find_element(By.TAG_NAME, "strong")
        self.assertEqual(element.text, "Viewing All Recipes 🍴")
        currURL = self.driver.current_url
        self.assertEqual(currURL, self.link + "recipes")

    def testAbout(self):
        self.driver.find_element(By.LINK_TEXT, "About").click()
        element = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(element.text, "About Us")
        currURL = self.driver.current_url
        self.assertEqual(currURL, self.link + "about")


if __name__ == "__main__":
    unittest.main()
