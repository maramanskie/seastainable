import unittest
from urllib import response
import requests
import json

API_ENDPOINT_DEPLOY = "https://api.seastainable.me"
API_ENDPOINT_LOCAL = "http://localhost:5000"
API_ENDPOINT = API_ENDPOINT_DEPLOY


class TestSeastainableAPI(unittest.TestCase):

    # MarineLife Model Endpoint Tests

    def testAllEndpointMarineLife(self):
        response = requests.get(API_ENDPOINT + "/marinelife/all")
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson), 92)

    # filter harvestType=Farmed, filter calories >= 100
    def testSearchSortFilterMarineLifeNoResults(self):
        response = requests.get(
            API_ENDPOINT
            + "/marinelife?page=1&instancesPerPage=5&harvestType=Farmed&calories=gte:100"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson['data']), 0)

    # sort calories ascending
    def testSearchSortFilterMarineLifeSortAscending(self):
        response = requests.get(
            API_ENDPOINT + "/marinelife?page=2&instancesPerPage=5&sort=calories:asc"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson['count'], 92)
        data = responseJson['data']
        self.assertEqual(len(data), 5)
        expectedCaloriesOrder = [81, 82, 82, 84, 85]
        actualCaloriesOrder = [d["caloriesVal"] for d in data]
        self.assertEqual(expectedCaloriesOrder, actualCaloriesOrder)

    # sort calories descending
    def testSearchSortFilterMarineLifeSortDescending(self):
        response = requests.get(
            API_ENDPOINT + "/marinelife?page=3&instancesPerPage=5&sort=calories:desc"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson['count'], 92)
        data = responseJson['data']
        self.assertEqual(len(data), 5)
        expectedCaloriesOrder = [150, 146, 144, 144, 142]
        actualCaloriesOrder = [d["caloriesVal"] for d in data]
        self.assertEqual(expectedCaloriesOrder, actualCaloriesOrder)

    # search: salmon
    def testSearchSortFilterMarineLifeSearchCommonName(self):
        response = requests.get(
            API_ENDPOINT + "/marinelife?page=3&instancesPerPage=4&search=salmon"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson['count'], 6)
        data = responseJson['data']
        self.assertEqual(len(data), 2)

    # search: Thunnus alalunga
    def testSearchSortFilterMarineLifeSearchScientificName(self):
        response = requests.get(
            API_ENDPOINT
            + "/marinelife?page=1&instancesPerPage=1&search=Thunnus+alalunga"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson['count'], 8)
        data = responseJson['data']
        self.assertEqual(len(data), 1)

    # filter harvestType=wild
    def testSearchSortFilterMarineLifeFilterHarvestType(self):
        response = requests.get(
            API_ENDPOINT + "/marinelife?page=1&instancesPerPage=3&harvestType=Wild"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson['count'], 87)
        data = responseJson['data']
        self.assertEqual(len(data), 3)
        expectedValues = ["Wild"] * 3
        actualValues = [d["harvestType"] for d in data]
        self.assertEqual(expectedValues, actualValues)

    # filter populationStatus=unknown
    def testSearchSortFilterMarineLifeFilterPopulationStatus(self):
        response = requests.get(
            API_ENDPOINT
            + "/marinelife?page=3&instancesPerPage=2&populationStatus=unknown"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson['count'], 19)
        data = responseJson['data']
        self.assertEqual(len(data), 2)
        self.assertEqual("unknown" in data[0]["populationStatus"], True)
        self.assertEqual("unknown" in data[0]["populationStatus"], True)

    # filter calories >= 100, search above, sort calories desc
    def testSearchSortFilterMarineLifeCombo(self):
        response = requests.get(
            API_ENDPOINT
            + "/marinelife?page=4&instancesPerPage=3&calories=gte:100&search=above&sort=calories:desc"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson['count'], 31)
        data = responseJson['data']
        self.assertEqual(len(data), 3)
        expectedValues = [130] * 3
        actualValues = [d["caloriesVal"] for d in data]
        self.assertEqual(expectedValues, actualValues)

    def testIdEndpointMarineLifeSuccess(self):
        response = requests.get(API_ENDPOINT + "/marinelife/id/45")
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson), 1)
        responseJson = responseJson[0]
        self.assertEqual(responseJson["caloriesVal"], 144)
        self.assertEqual(responseJson["calories"], "144 calories")
        self.assertEqual(responseJson["commonName"], "Pacific Bluefin Tuna")
        self.assertEqual(responseJson["harvestType"], "Wild")
        self.assertEqual(responseJson["locationMap"], "Pacific+Ocean")
        self.assertEqual(responseJson["regions"], "California coast")
        self.assertEqual(responseJson["scientificName"], "Thunnus orientalis")
        self.assertEqual(responseJson["waterBody"], "Pacific Ocean")

    def testIdEndpointMarineLifeFail(self):
        responseJson = requests.get(API_ENDPOINT + "/marinelife/id/100").json()
        self.assertEqual(
            responseJson["status"], "Could not find MarineLife instance with given id"
        )

    def testNameEndpointMarineLifeSuccess(self):
        response = requests.get(API_ENDPOINT + "/marinelife/name/Sockeye+Salmon")
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson), 1)
        responseJson = responseJson[0]
        self.assertEqual(responseJson['caloriesVal'], 168)
        self.assertEqual(responseJson['calories'], '168 calories')
        self.assertEqual(responseJson['commonName'], "Sockeye Salmon")
        self.assertEqual(responseJson['harvestType'], "Wild")
        self.assertEqual(responseJson['locationMap'], "Pacific+Ocean")
        self.assertEqual(responseJson['regions'], "Northwest Alaska to the Deschutes River in Oregon")
        self.assertEqual(responseJson['scientificName'], "Oncorhynchus nerka")
        self.assertEqual(responseJson['waterBody'], "Pacific Ocean")
    
    def testNameEndpointMarineLifeFail(self):
        responseJson = requests.get(API_ENDPOINT + "/marinelife/name/Bird").json()
        self.assertEqual(
            responseJson["status"], "Could not find MarineLife instance with given name"
        )

    # Ingredients Model Endpoint Tests

    def testAllEndpointIngredients(self):
        response = requests.get(API_ENDPOINT + "/ingredients/all")
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson), 459)

    # filter consisstency=liquid and search meat
    def testSearchSortFilterIngredientsNoResults(self):
        response = requests.get(
            API_ENDPOINT
            + "/ingredients?page=3&instancesPerPage=2&consistency=liquid&search=Meat"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 0)
        data = responseJson["data"]
        self.assertEqual(len(data), 0)

    # sort percent fat ascending
    def testSearchSortFilterIngredientsSortAscending(self):
        response = requests.get(
            API_ENDPOINT + "/ingredients?page=3&instancesPerPage=4&sort=percentFat:asc"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 459)
        data = responseJson["data"]
        self.assertEqual(len(data), 4)
        expectedValues = [0.0] * 4
        actualValues = [d["percentFatVal"] for d in data]
        self.assertEqual(expectedValues, actualValues)

    # sort percent fat descending
    def testSearchSortFilterIngredientsSortDescending(self):
        response = requests.get(
            API_ENDPOINT
            + "/ingredients?page=10&instancesPerPage=4&sort=percentFat:desc"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 459)
        data = responseJson["data"]
        self.assertEqual(len(data), 4)
        expectedValues = [86.06, 85.17, 83.54, 81.19]
        actualValues = [d["percentFatVal"] for d in data]
        self.assertEqual(expectedValues, actualValues)

    # search for Produce
    def testSearchSortFilterIngredientsSearchAisle(self):
        response = requests.get(
            API_ENDPOINT + "/ingredients?page=2&instancesPerPage=5&search=Produce"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 101)
        data = responseJson["data"]
        self.assertEqual(len(data), 5)

    # search for baby zucchinis
    def testSearchSortFilterIngredientsSearchName(self):
        response = requests.get(
            API_ENDPOINT
            + "/ingredients?page=3&instancesPerPage=5&search=baby+zucchinis"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 4)
        data = responseJson["data"]
        self.assertEqual(len(data), 4)
        expectedIds = {12, 158, 253, 384}
        actualIds = {d["id"] for d in data}
        self.assertEqual(expectedIds, actualIds)

    # filter by consistency=solid
    def testSearchSortFilterIngredientsFilterConsistency(self):
        response = requests.get(
            API_ENDPOINT + "/ingredients?page=3&instancesPerPage=3&consistency=solid"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 386)
        data = responseJson["data"]
        self.assertEqual(len(data), 3)
        expectedValues = ["solid"] * 3
        actualValues = [d["consistency"] for d in data]
        self.assertEqual(expectedValues, actualValues)

    # filter by price >= 500
    def testSearchSortFilterIngredientsFilterPrice(self):
        response = requests.get(
            API_ENDPOINT + "/ingredients?page=3&instancesPerPage=6&price=gte:500"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 24)
        data = responseJson["data"]
        self.assertEqual(len(data), 6)
        self.assertTrue(all(d["priceVal"] >= 500 for d in data))

    # filter consistency=liquid, search beverages, sort price desc
    def testSearchSortFilterIngredientsCombo(self):
        response = requests.get(
            API_ENDPOINT
            + "/ingredients?page=3&instancesPerPage=5&consistency=liquid&search=beverages&sort=price:desc"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 15)
        data = responseJson["data"]
        self.assertEqual(len(data), 5)

    def testIdEndpointIngredientsSuccess(self):
        response = requests.get(API_ENDPOINT + "/ingredients/id/343")
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson), 1)
        responseJson = responseJson[0]
        self.assertEqual(responseJson["aisle"], "Refrigerated")
        self.assertEqual(responseJson["consistency"], "solid")
        self.assertEqual(responseJson["ingName"], "refrigerated pizza dough")
        nutritionDict = json.loads(responseJson["nutrition"])
        self.assertEqual(nutritionDict["caloricBreakdown"]["percentProtein"], 12.34)
        self.assertEqual(nutritionDict["caloricBreakdown"]["percentFat"], 11.18)
        self.assertEqual(nutritionDict["caloricBreakdown"]["percentCarbs"], 76.48)
        self.assertEqual(responseJson["weightPerServing"], "391 g")
        self.assertEqual(responseJson["percentProtein"], "12.34%")
        self.assertEqual(responseJson["percentFat"], "11.18%")
        self.assertEqual(responseJson["percentFatVal"], 11.18)
        self.assertEqual(responseJson["percentCarbs"], "76.48%")
        self.assertEqual(responseJson["priceVal"], 335.14)
        self.assertEqual(responseJson["price"], "$3.35")
        self.assertNotEqual(len(responseJson["recipes"]), 0)
        self.assertNotEqual(len(responseJson["fishType"]), 0)

    def testIdEndpointIngredientFail(self):
        responseJson = requests.get(API_ENDPOINT + "/ingredients/id/1000").json()
        self.assertEqual(
            responseJson["status"], "Could not find Ingredients instance with given id"
        )

    def testNameEndpointIngredientsSuccess(self):
        response = requests.get(API_ENDPOINT + "/ingredients/name/baby+zucchinis")
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson), 1)
        responseJson = responseJson[0]
        self.assertEqual(responseJson["aisle"], "Produce")
        self.assertEqual(responseJson["consistency"], "solid")
        self.assertEqual(responseJson["ingName"], "baby zucchinis")
        nutritionDict = json.loads(responseJson["nutrition"])
        self.assertEqual(nutritionDict["caloricBreakdown"]["percentProtein"], 40.33)
        self.assertEqual(nutritionDict["caloricBreakdown"]["percentFat"], 13.39)
        self.assertEqual(nutritionDict["caloricBreakdown"]["percentCarbs"], 46.28)
        self.assertEqual(responseJson["weightPerServing"], "196 g")
        self.assertEqual(responseJson["percentProtein"], "40.33%")
        self.assertEqual(responseJson["percentFat"], "13.39%")
        self.assertEqual(responseJson["percentFatVal"], 13.39)
        self.assertEqual(responseJson["percentCarbs"], "46.28%")
        self.assertEqual(responseJson["priceVal"], 392)
        self.assertEqual(responseJson["price"], "$3.92")
        self.assertNotEqual(len(responseJson["recipes"]), 0)
        self.assertNotEqual(len(responseJson["fishType"]), 0)

    def testNameEndpointIngredientsFail(self):
        responseJson = requests.get(
            API_ENDPOINT + "/ingredients/name/purple+red"
        ).json()
        self.assertEqual(
            responseJson["status"],
            "Could not find Ingredients instance with given name",
        )

    # Recipes Model Endpoint Tests

    def testAllEndpointRecipes(self):
        response = requests.get(API_ENDPOINT + "/recipes/all")
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson), 330)

    # filter rating=38, servings=1
    def testSearchSortFilterRecipesNoResults(self):
        response = requests.get(
            API_ENDPOINT + "/recipes?page=10&instancesPerPage=4&rating=38&numServings=1"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 0)
        data = responseJson["data"]
        self.assertEqual(len(data), 0)

    # sort prep time ascending
    def testSearchSortFilterRecipesSortAscending(self):
        response = requests.get(
            API_ENDPOINT + "/recipes?page=10&instancesPerPage=5&sort=prepTime:asc"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 330)
        data = responseJson["data"]
        self.assertEqual(len(data), 5)
        expectedValues = [10, 11, 12, 13, 13]
        actualValues = [d["prepTimeVal"] for d in data]
        self.assertEqual(expectedValues, actualValues)

    # sort prep time descending
    def testSearchSortFilterRecipesSortDescending(self):
        response = requests.get(
            API_ENDPOINT + "/recipes?page=5&instancesPerPage=5&sort=prepTime:desc"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 330)
        data = responseJson["data"]
        self.assertEqual(len(data), 5)
        expectedValues = [85, 83, 80, 77, 75]
        actualValues = [d["prepTimeVal"] for d in data]
        self.assertEqual(expectedValues, actualValues)

    # search Citrus Alaskan Halibut
    def testSearchSortFilterRecipesSearchName(self):
        response = requests.get(
            API_ENDPOINT
            + "/recipes?page=3&instancesPerPage=2&search=Citrus+Alaskan+Halibut"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 11)
        data = responseJson["data"]
        self.assertEqual(len(data), 2)

    # filter rating > 50
    def testSearchSortFilterRecipesFilterRating(self):
        response = requests.get(
            API_ENDPOINT + "/recipes?page=5&instancesPerPage=4&rating=gt:50"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 210)
        data = responseJson["data"]
        self.assertEqual(len(data), 4)
        self.assertTrue(all(d["ratingVal"] > 50 for d in data))

    # filter servings = 1
    def testSearchSortFilterRecipesFilterServings(self):
        response = requests.get(
            API_ENDPOINT + "/recipes?page=2&instancesPerPage=6&numServings=1"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 44)
        data = responseJson["data"]
        self.assertEqual(len(data), 6)
        expectedValues = [1] * 6
        actualValues = [d["numServingsVal"] for d in data]
        self.assertEqual(expectedValues, actualValues)

    # search salmon, filter servings > 1, sort cost desc
    # second page results are equivalent to first page since total results is 4
    def testSearchSortFilterRecipesCombo(self):
        response = requests.get(
            API_ENDPOINT
            + "/recipes?page=2&instancesPerPage=6&numServings=gt:1&search=salmon&sort=cost:desc"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(responseJson["count"], 4)
        data = responseJson["data"]
        self.assertEqual(len(data), 4)
        expectedIds = [131, 127, 126, 125]
        actualIds = [d["id"] for d in data]
        self.assertEqual(expectedIds, actualIds)

    def testIdEndpointRecipesSuccess(self):
        response = requests.get(API_ENDPOINT + "/recipes/id/21")
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson), 1)
        responseDict = responseJson[0]
        self.assertEqual(responseDict["name"], "Smoked Lobster Tails")
        self.assertEqual(responseDict["numServingsVal"], 4)
        self.assertEqual(responseDict["numServings"], "4 servings")
        self.assertEqual(responseDict["prepTimeVal"], 30)
        self.assertEqual(responseDict["prepTime"], "30 minutes")
        self.assertEqual(responseDict["ratingVal"], 44)
        self.assertEqual(responseDict["rating"], "44/100")
        self.assertEqual(responseDict["cost"], "$5.45")
        self.assertEqual(responseDict["costVal"], 545.41)
        dietDict = json.loads(responseDict["diet"])
        self.assertEqual(dietDict["dairyFree"], False)
        self.assertEqual(dietDict["glutenFree"], True)
        self.assertEqual(dietDict["vegan"], False)
        self.assertEqual(dietDict["vegetarian"], False)
        self.assertNotEqual(len(responseDict["fishType"]), 0)
        self.assertNotEqual(len(responseDict["ingredients"]), 0)

    def testIdEndpointRecipesFail(self):
        responseJson = requests.get(API_ENDPOINT + "/recipes/id/331").json()
        self.assertEqual(
            responseJson["status"], "Could not find Recipes instance with given id"
        )

    def testNameEndpointRecipesSuccess(self):
        response = requests.get(
            API_ENDPOINT + "/recipes/name/Coconut+Curry+Dip+with+Shrimp"
        )
        self.assertEqual(response.status_code, 200)
        responseJson = response.json()
        self.assertEqual(len(responseJson), 1)
        responseDict = responseJson[0]
        self.assertEqual(responseDict["name"], "Coconut Curry Dip with Shrimp")
        self.assertEqual(responseDict["numServingsVal"], 8)
        self.assertEqual(responseDict["numServings"], "8 servings")
        self.assertEqual(responseDict["prepTimeVal"], 15)
        self.assertEqual(responseDict["prepTime"], "15 minutes")
        self.assertEqual(responseDict["ratingVal"], 19)
        self.assertEqual(responseDict["rating"], "19/100")
        self.assertEqual(responseDict["cost"], "$0.38")
        self.assertEqual(responseDict["costVal"], 38.03)
        dietDict = json.loads(responseDict["diet"])
        self.assertEqual(dietDict["dairyFree"], False)
        self.assertEqual(dietDict["glutenFree"], True)
        self.assertEqual(dietDict["vegan"], False)
        self.assertEqual(dietDict["vegetarian"], False)
        self.assertNotEqual(len(responseDict["fishType"]), 0)
        self.assertNotEqual(len(responseDict["ingredients"]), 0)

    def testNameEndpointRecipesFail(self):
        responseJson = requests.get(API_ENDPOINT + "/recipes/name/Pecan+Pie").json()
        self.assertEqual(
            responseJson["status"], "Could not find Recipes instance with given name"
        )


if __name__ == "__main__":
    unittest.main()
