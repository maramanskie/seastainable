from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer, or_
from dotenv import load_dotenv
import os
from flask_marshmallow import Marshmallow

# Init app
app = Flask(__name__)
CORS(app)

# Init db
load_dotenv()
db_password = os.getenv('DB_PASSWORD')
db_password = db_password if db_password else process.env.DB_PASSWORD
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql+psycopg2://seastainable:{db_password}@seastainabledb.ch7fochch5fm.us-east-1.rds.amazonaws.com:5432/postgres'
db = SQLAlchemy(app)
ma = Marshmallow(app)

# DB Specifications
# Note: some of these fields have HTML. Render as innerHTML instead of innerText.
class MarineLife(db.Model):
  __tablename__ = "MarineLifeTest"

  # arranged in the order a record presents them in the json
  id = db.Column(db.Integer, primary_key=True)
  populationStatus = db.Column(db.String())
  calories = db.Column(db.String())
  caloriesVal = db.Column(db.Integer)
  availability = db.Column(db.String())
  idFeatures = db.Column(db.String())
  commonName = db.Column(db.String(), unique=True)
  scientificName = db.Column(db.String())
  regions = db.Column(db.String())
  waterBody = db.Column(db.String())
  harvestType = db.Column(db.String())
  quote = db.Column(db.String())
  locationMap = db.Column(db.String())
  image = db.Column(db.String())
  ingredients = db.Column(db.String()) # JSON list of ingredients it appears alongside in recipes
  recipes = db.Column(db.String()) # JSON list of recipes it appears in

class Recipes(db.Model):
  __tablename__ = "RecipesTest"

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(), unique=True)
  image = db.Column(db.String())
  instructions = db.Column(db.String())
  rating = db.Column(db.String())
  ratingVal = db.Column(db.Integer)
  cost = db.Column(db.String()) 
  costVal = db.Column(db.Float)
  diet = db.Column(db.String())
  numServings = db.Column(db.String())
  numServingsVal = db.Column(db.Integer)
  prepTime = db.Column(db.String())
  prepTimeVal = db.Column(db.Integer)
  ingredients = db.Column(db.String())
  fishType = db.Column(db.String()) 

class Ingredients(db.Model):
  __tablename__ = "IngredientsTest"

  id = db.Column(db.Integer, primary_key=True)
  image = db.Column(db.String())
  price = db.Column(db.String())
  priceVal = db.Column(db.Float)
  aisle = db.Column(db.String())        
  consistency = db.Column(db.String())
  ingName = db.Column(db.String(), unique=True)
  fishType = db.Column(db.String())
  nutrition = db.Column(db.String())
  recipes = db.Column(db.String())
  weightPerServing = db.Column(db.String())
  percentProtein = db.Column(db.String())
  percentFat = db.Column(db.String())
  percentFatVal = db.Column(db.Float)
  percentCarbs = db.Column(db.String())
  media = db.Column(db.String())

db.create_all()

# Schemas: referenced Marshmallow documentation and https://gitlab.com/vijaykvuyyuru/swe-college-football-project/-/blob/dev/backend/models.py

class MarineLifeSchema(ma.SQLAlchemySchema):
  class Meta:
    model = MarineLife
  
  id = ma.auto_field()
  populationStatus = ma.auto_field()
  calories = ma.auto_field()
  caloriesVal = ma.auto_field()
  availability = ma.auto_field()
  idFeatures = ma.auto_field()
  commonName = ma.auto_field()
  scientificName = ma.auto_field()
  regions = ma.auto_field()
  waterBody = ma.auto_field()
  harvestType = ma.auto_field()
  quote = ma.auto_field()
  locationMap = ma.auto_field()
  image = ma.auto_field()
  ingredients = ma.auto_field()
  recipes = ma.auto_field()

class IngredientsSchema(ma.SQLAlchemySchema):
  class Meta:
    model = Ingredients
  
  id = ma.auto_field()
  image = ma.auto_field()
  price = ma.auto_field()
  priceVal = ma.auto_field()
  aisle = ma.auto_field()
  consistency = ma.auto_field()
  ingName = ma.auto_field()
  fishType = ma.auto_field()
  nutrition = ma.auto_field()
  recipes = ma.auto_field()
  weightPerServing = ma.auto_field()
  percentProtein = ma.auto_field()
  percentFat = ma.auto_field()
  percentFatVal = ma.auto_field()
  percentCarbs = ma.auto_field()
  media = ma.auto_field()

class RecipesSchema(ma.SQLAlchemySchema):
  class Meta:
    model = Recipes
  
  id = ma.auto_field()
  name = ma.auto_field()
  image = ma.auto_field()
  instructions = ma.auto_field()
  rating = ma.auto_field()
  ratingVal = ma.auto_field()
  cost = ma.auto_field()
  costVal = ma.auto_field()
  diet = ma.auto_field()
  numServings = ma.auto_field()
  numServingsVal = ma.auto_field()
  prepTime = ma.auto_field()
  prepTimeVal = ma.auto_field()
  ingredients = ma.auto_field()
  fishType = ma.auto_field()