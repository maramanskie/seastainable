# Backend

**NOTE** anything related to virtual envs is still relevant for helping to isolate/manage dependencies!
However, the runtime can be built automatically without any of this set up by simply running docker.
For windows the following should do the trick

Dev:
```
docker build -t flask-docker-dev -f dev.Dockerfile .
docker run -it -v ${PWD}:/usr/src/backend -w /usr/src/backend -p 5000:5000 flask-docker-dev
```

For Mac, the second command will be:
```
docker run -it -v $pwd:/usr/src/backend -w /usr/src/backend -p 5000:5000 flask-docker-dev
```

When built locally, this should be accessible from `localhost:5000` on your machine.
The output to the screen when Flask starts may seem like a different host, and that is the case
because it is running within the docker container. These commands will ensure that its 5000 port 
gets mapped to the machine that is running the container.

"Prod":
```
docker build -t flask-docker -f Dockerfile .
docker run -it -v ${PWD}:/usr/src/backend -w /usr/src/backend -p 80:80 flask-docker
```

For Mac, the second command will be:
```
docker run -it -v $pwd:/usr/src/backend -w /usr/src/backend -p 80:80 flask-docker
```

Access from `localhost:80` on your machine

**Secrets**: Locally accessed using dotenv, make sure to have backend/.env in your local copy. Make sure to not push this to git.
Deployments know about the secrets by configuration in Elastic Beanstalk.

Leverages Flask to build a backend API.

It is worth noting that the backend should run in a virtual environment (so that any dependencies
can be easily booted up on anyone's machine). Please perform the following when developing.
- Install virtual environment using `pip install virtualenv`. Add the `--user` flag if you are experiencing errors.
- **From within this backend directory** run `virtualenv env` to create a virtual environment. `py -m virtualenv env` if that doesn't work.
- A directory called `env` should appear. Within it is a `.gitignore` file. Please verify that this is the case. If not, please add one,
and fill it with a single asterisk (*) so that it will ignore everything in the virtual env folder. Please do not remove this.
- On Windows, running `.\env\Scripts\activate` from this backend directory should boot you into the
virtual environment. 
- `env/Scripts/activate` should work on other systems (or however you can get to and run this activate script)

**PIP** (package manager for Python) is used to keep track of our dependencies.Running `pip freeze` will output our 
dependencies and versions. Let's redirect this output into disk using `pip freeze > requirements.txt`. 
Try to do this often, in particular, whenever a new package or version is installed.

**From within the virtual environment** run `pip install -r requirements.txt` or `py -m pip install -r requirements.txt`.
pip will installthe necessary dependencies within this environment, referencing the frozen dependencies in this file.
Run often as well, in case our dependencies change or upgrade.

If you did everything correctly, you're good to run `py backend.py` to start the backend server. 

Run `deactivate` from within the virtual environment to exit.

Necessary deps:
- Flask
- Flask cors(cross origin resource sharing) so we can call from the frontend