from collections import defaultdict
import json
from flask import jsonify, request
from models import *

# Define endpoints 

# GET example. Can also use parameters in the endpoint. Typically for querying
@app.route(
    "/"
)  # (can remove) this is a decorator, think of it like "wrapping" this function in another
def hello():
    # route "/" is the base URI so accessing localhost:5000 should return this
    # consider JSON responses for future endpoints to be parsed out by the frontend.
    return "Hello World!"


@app.route("/devs")
def developers():
    return '["Caleb", "Carlos", "Derrick", "Geethika", "Mara"]'


# API Endpoints

# URL converter syntax referenced from: https://gitlab.com/daniamir/sweproject/-/blob/dev/backend/app.py

# Shared functions/objects

marineLifeSchema = MarineLifeSchema(many=True)
ingredientsSchema = IngredientsSchema(many=True)
recipesSchema = RecipesSchema(many=True)


def paginationParamsAbsent(params):
    return (
        (len(params) == 0)
        or ("instancesPerPage" not in params and "page" not in params)
        or ("instancesPerPage" not in params and "page" in params)
        or ("instancesPerPage" in params and "page" not in params)
    )


def getPageData(instancesPerPage, numInstances, pageNum):
    numPages = (numInstances // instancesPerPage) + (
        1 if numInstances % instancesPerPage != 0 else 0
    )
    # Input checking
    if pageNum > numPages:
        pageNum = numPages
    startId = instancesPerPage * (pageNum - 1) + 1
    endId = (
        startId + instancesPerPage - 1
        if startId <= numInstances - instancesPerPage - 1
        else numInstances
    )
    return {"startId": startId, "endId": endId}


def sortData(query, sortParams, table, attributeDict):
    attributeSplit = sortParams.split(":")
    attributeName = attributeSplit[0]
    sortOrder = attributeSplit[1]
    attributeCol = attributeDict[attributeName]

    if sortOrder == "asc":
        query = db.session.query(table).order_by(attributeCol)
    elif sortOrder == "desc":
        query = db.session.query(table).order_by(attributeCol.desc())
    else:
        return "Invalid sort order"

    return query


def parseNumericFilters(attribute, filterVal):
    filter = filterVal.split(":")
    if len(filter) == 1:
        filterRule = attribute == filterVal
    else:
        inequality = filter[0]
        value = filter[1]

        if inequality == "gt":
            filterRule = attribute > value
        elif inequality == "gte":
            filterRule = attribute >= value
        elif inequality == "lt":
            filterRule = attribute < value
        elif inequality == "lte":
            filterRule = attribute <= value
        else:  # inequality is assumed to be neq
            filterRule = attribute != value

    return filterRule


def getFilterQuery(query, filterRule, table):
    if query is None:
        query = db.session.query(table).filter(filterRule)
    else:
        query = query.filter(filterRule)

    return query


def getSearchQuery(query, searchQueries, table):
    if query is None:
        query = db.session.query(table).filter(or_(*searchQueries))
    else:
        query = query.filter(or_(*searchQueries))

    return query


# Search algorithm/code inspired by: https://gitlab.com/alex.chandler/GerryMap/-/blob/development/backend/search.py
def searchStringAttributes(keywords, attributes):
    searchQueries = []
    for attribute in attributes:
        for i in range(len(keywords)):
            for j in range(i + 1, len(keywords) + 1):
                searchQueries.append(
                    attribute.ilike("%" + " ".join(keywords[i:j]) + "%")
                )

    return searchQueries


# Marine Life

marineLifeColDict = {
    "populationStatus": MarineLife.populationStatus,
    "calories": MarineLife.caloriesVal,
    "caloriesString": MarineLife.calories,
    "availability": MarineLife.availability,
    "idFeatures": MarineLife.idFeatures,
    "commonName": MarineLife.commonName,
    "scientificName": MarineLife.scientificName,
    "regions": MarineLife.regions,
    "waterBody": MarineLife.waterBody,
    "harvestType": MarineLife.harvestType,
    "quote": MarineLife.quote,
}


def sortMarineLife(query, sortParams):
    return sortData(query, sortParams, MarineLife, marineLifeColDict)


def filterMarineLife(query, attribute, filterVal):
    filterRule = None

    if attribute == MarineLife.harvestType:
        filterRule = attribute == filterVal
    elif attribute == MarineLife.populationStatus:
        filterRule = MarineLife.populationStatus.ilike("%" + filterVal + "%")
    else:  # attribute assumed to be calories
        filterRule = parseNumericFilters(MarineLife.caloriesVal, filterVal)

    return getFilterQuery(query, filterRule, MarineLife)


def searchMarineLife(query, attributes, searchString):
    keywords = searchString.split(" ")
    searchQueries = searchStringAttributes(keywords, attributes)
    return getSearchQuery(query, searchQueries, MarineLife)


# Get all marine life instances
@app.route("/marinelife/all")
def marineLifeAll():
    allQuery = db.session.query(MarineLife).all()
    allQueryJson = marineLifeSchema.dump(allQuery)
    return jsonify(allQueryJson)


# Get marine life instances based on a number of parameters
# URL format example: https://api.seastainable.me/marinelife?page=3&instancesPerPage=2&sort=calories:desc
# Url params: page, instancesPerPage, sort=key1:asc/desc, filters, search
@app.route("/marinelife")
def instancesForPageMarineLife():
    params = request.args
    if paginationParamsAbsent(params):
        return jsonify({"status": "Must specify both instancesPerPage and page"})
    else:
        instancesPerPage = int(params.get("instancesPerPage"))
        numInstances = db.session.query(MarineLife).count()
        pageNum = int(params.get("page"))

        if len(params) == 2:
            # no search/sort/filter
            # Normal pagination
            pageDataFull = getPageData(instancesPerPage, numInstances, pageNum)
            pageQuery = db.session.query(MarineLife).filter(
                MarineLife.id >= pageDataFull["startId"],
                MarineLife.id <= pageDataFull["endId"],
            )
            pageQueryJson = marineLifeSchema.dump(pageQuery)
            return jsonify({"count": numInstances, "data": pageQueryJson})
        else:
            # search/sort/filter present
            query = None
            # sort based on val
            if "sort" in params:
                sortParams = params.get("sort")
                query = sortMarineLife(query, sortParams)
                if query == "Invalid sort order":
                    return jsonify({"status": "Invalid sort order"})

            # filter - harvestType, populationStatus, calories
            if "harvestType" in params:
                query = filterMarineLife(
                    query, MarineLife.harvestType, params.get("harvestType")
                )

            if "populationStatus" in params:
                query = filterMarineLife(
                    query, MarineLife.populationStatus, params.get("populationStatus")
                )

            if "calories" in params:
                query = filterMarineLife(
                    query, MarineLife.caloriesVal, params.get("calories")
                )

            # search based on string
            if "search" in params:
                searchParams = params.get("search")
                attributes = [
                    marineLifeColDict["commonName"],
                    marineLifeColDict["scientificName"],
                    marineLifeColDict["harvestType"],
                    marineLifeColDict["populationStatus"],
                    marineLifeColDict["caloriesString"],
                ]
                query = searchMarineLife(query, attributes, searchParams)

            numResults = query.count()
            queryJson = marineLifeSchema.dump(query)
            pageData = getPageData(instancesPerPage, numResults, pageNum)
            data = queryJson[pageData["startId"] - 1 : pageData["endId"]]
            return jsonify({"count": numResults, "data": data})


# Get marine life info for a given id
@app.route("/marinelife/id/<int:id>")
def marineLifeById(id):
    idObj = db.session.query(MarineLife).filter(MarineLife.id == id)
    idObjJson = marineLifeSchema.dump(idObj)

    if len(idObjJson) == 0:
        return jsonify({"status": "Could not find MarineLife instance with given id"})
    else:
        return jsonify(idObjJson)


# Get marine life info for a specific (common) name
@app.route("/marinelife/name/<string:name>")
def marineLifeByName(name):
    parsedName = name.replace("+", " ")
    nameQuery = db.session.query(MarineLife).filter(MarineLife.commonName == parsedName)
    nameQueryJson = marineLifeSchema.dump(nameQuery)

    if len(nameQueryJson) == 0:
        return jsonify({"status": "Could not find MarineLife instance with given name"})
    else:
        return jsonify(nameQueryJson)


# Ingredients

ingredientsColDict = {
    "priceString": Ingredients.price,
    "price": Ingredients.priceVal,
    "aisle": Ingredients.aisle,
    "consistency": Ingredients.consistency,
    "ingName": Ingredients.ingName,
    "nutrition": Ingredients.nutrition,
    "weightPerServing": Ingredients.weightPerServing,
    "percentFatString": Ingredients.percentFat,
    "percentFat": Ingredients.percentFatVal,
}


def sortIngredients(query, sortParams):
    return sortData(query, sortParams, Ingredients, ingredientsColDict)


def filterIngredients(query, attribute, filterVal):
    filterRule = None
    if attribute is Ingredients.consistency:
        filterRule = attribute == filterVal
    elif attribute is Ingredients.aisle:
        filterRule = attribute.ilike("%" + filterVal + "%")
    else:  # filtering by price or percentFat (numeric attributes)
        filterRule = parseNumericFilters(attribute, filterVal)

    return getFilterQuery(query, filterRule, Ingredients)


def searchIngredients(query, attributes, searchString):
    keywords = searchString.split(" ")
    searchQueries = searchStringAttributes(keywords, attributes)
    return getSearchQuery(query, searchQueries, Ingredients)


# Get all ingredient instances
@app.route("/ingredients/all")
def ingredientsAll():
    allQuery = db.session.query(Ingredients).all()
    allQueryJson = ingredientsSchema.dump(allQuery)
    return jsonify(allQueryJson)


# Get ingredients instances based on a number of parameters
# URL format example: https://api.seastainable.me/ingredients?page=3&instancesPerPage=2&sort=price:asc
# Url params: page, instancesPerPage, sort=key1:asc/desc, filters, search
@app.route("/ingredients")
def instancesForPageIngredients():
    params = request.args
    if paginationParamsAbsent(params):
        return jsonify({"status": "Must specify both instancesPerPage and page"})
    else:
        instancesPerPage = int(params.get("instancesPerPage"))
        numInstances = db.session.query(Ingredients).count()
        pageNum = int(params.get("page"))

        if len(params) == 2:
            # no search/sort/filter
            # Normal pagination
            pageDataFull = getPageData(instancesPerPage, numInstances, pageNum)
            pageQuery = db.session.query(Ingredients).filter(
                Ingredients.id >= pageDataFull["startId"],
                Ingredients.id <= pageDataFull["endId"],
            )
            pageQueryJson = ingredientsSchema.dump(pageQuery)
            return jsonify({"count": numInstances, "data": pageQueryJson})
        else:
            # search/sort/filter present
            query = None
            # sort based on val
            if "sort" in params:
                sortParams = params.get("sort")
                query = sortIngredients(query, sortParams)
                if query == "Invalid sort order":
                    return jsonify({"status": "Invalid sort order"})

            # filter - consistency, aisle, price, percent fat
            if "consistency" in params:
                query = filterIngredients(
                    query, Ingredients.consistency, params.get("consistency")
                )

            if "aisle" in params:
                query = filterIngredients(query, Ingredients.aisle, params.get("aisle"))

            if "price" in params:
                query = filterIngredients(
                    query, Ingredients.priceVal, params.get("price")
                )

            if "percentFat" in params:
                query = filterIngredients(
                    query, Ingredients.percentFatVal, params.get("percentFat")
                )

            # search based on string
            if "search" in params:
                searchParams = params.get("search")
                attributes = [
                    ingredientsColDict["consistency"],
                    ingredientsColDict["ingName"],
                    ingredientsColDict["aisle"],
                    ingredientsColDict["priceString"],
                    ingredientsColDict["percentFatString"],
                ]
                query = searchIngredients(query, attributes, searchParams)

            numResults = query.count()
            queryJson = ingredientsSchema.dump(query)
            pageData = getPageData(instancesPerPage, len(queryJson), pageNum)
            data = queryJson[pageData["startId"] - 1 : pageData["endId"]]
            return jsonify({"count": numResults, "data": data})


# Get ingredient info for a given id
@app.route("/ingredients/id/<int:id>")
def ingredientById(id):
    idObj = db.session.query(Ingredients).filter(Ingredients.id == id)
    idObjJson = ingredientsSchema.dump(idObj)

    if len(idObjJson) == 0:
        return jsonify({"status": "Could not find Ingredients instance with given id"})
    else:
        return jsonify(idObjJson)


# Get ingredient info for a given name
@app.route("/ingredients/name/<string:name>")
def ingredientByName(name):
    parsedName = name.replace("+", " ")
    nameQuery = db.session.query(Ingredients).filter(Ingredients.ingName == parsedName)
    nameQueryJson = ingredientsSchema.dump(nameQuery)

    if len(nameQueryJson) == 0:
        return jsonify(
            {"status": "Could not find Ingredients instance with given name"}
        )
    else:
        return jsonify(nameQueryJson)


# Recipes

recipesColDict = {
    "name": Recipes.name,
    "ratingString": Recipes.rating,
    "rating": Recipes.ratingVal,
    "costString": Recipes.cost,
    "cost": Recipes.costVal,
    "numServingsString": Recipes.numServings,
    "numServings": Recipes.numServingsVal,
    "prepTimeString": Recipes.prepTime,
    "prepTime": Recipes.prepTimeVal,
}


def sortRecipes(query, sortParams):
    return sortData(query, sortParams, Recipes, recipesColDict)


def filterRecipes(query, attribute, filterVal):
    filterRule = parseNumericFilters(attribute, filterVal)
    return getFilterQuery(query, filterRule, Recipes)


def searchRecipes(query, attributes, searchString):
    keywords = searchString.split(" ")
    searchQueries = searchStringAttributes(keywords, attributes)
    return getSearchQuery(query, searchQueries, Recipes)


# Get all recipe instances
@app.route("/recipes/all")
def recipesAll():
    allQuery = db.session.query(Recipes).all()
    allQueryJson = recipesSchema.dump(allQuery)
    return jsonify(allQueryJson)


# Get recipes instances based on a number of parameters
# URL format example: https://api.seastainable.me/recipes?page=3&instancesPerPage=2&sort=rating:asc
# Url params: page, instancesPerPage, sort=key1:asc/desc, filters, search
@app.route("/recipes")
def instancesForPageRecipes():
    params = request.args
    if paginationParamsAbsent(params):
        return jsonify({"status": "Must specify both instancesPerPage and page"})
    else:
        instancesPerPage = int(params.get("instancesPerPage"))
        numInstances = db.session.query(Recipes).count()
        pageNum = int(params.get("page"))

        if len(params) == 2:
            # no search/sort/filter
            # Normal pagination
            pageDataFull = getPageData(instancesPerPage, numInstances, pageNum)
            pageQuery = db.session.query(Recipes).filter(
                Recipes.id >= pageDataFull["startId"],
                Recipes.id <= pageDataFull["endId"],
            )
            pageQueryJson = recipesSchema.dump(pageQuery)
            return jsonify({"count": numInstances, "data": pageQueryJson})
        else:
            # search/sort/filter present
            query = None
            # sort based on val
            if "sort" in params:
                sortParams = params.get("sort")
                query = sortRecipes(query, sortParams)
                if query == "Invalid sort order":
                    return jsonify({"status": "Invalid sort order"})

            # filter - rating, cost, servings, prep time
            if "rating" in params:
                query = filterRecipes(query, Recipes.ratingVal, params.get("rating"))

            if "cost" in params:
                query = filterRecipes(query, Recipes.costVal, params.get("cost"))

            if "numServings" in params:
                query = filterRecipes(
                    query, Recipes.numServingsVal, params.get("numServings")
                )

            if "prepTime" in params:
                query = filterRecipes(
                    query, Recipes.prepTimeVal, params.get("prepTime")
                )

            # search based on string
            if "search" in params:
                searchParams = params.get("search")
                attributes = [
                    recipesColDict["name"],
                    recipesColDict["ratingString"],
                    recipesColDict["costString"],
                    recipesColDict["numServingsString"],
                    recipesColDict["prepTimeString"],
                ]
                query = searchRecipes(query, attributes, searchParams)

            numResults = query.count()
            queryJson = recipesSchema.dump(query)
            pageData = getPageData(instancesPerPage, len(queryJson), pageNum)
            data = queryJson[pageData["startId"] - 1 : pageData["endId"]]
            return jsonify({"count": numResults, "data": data})


# Get recipe info for a given id
@app.route("/recipes/id/<int:id>")
def recipeById(id):
    idObj = db.session.query(Recipes).filter(Recipes.id == id)
    idObjJson = recipesSchema.dump(idObj)

    if len(idObjJson) == 0:
        return jsonify({"status": "Could not find Recipes instance with given id"})
    else:
        return jsonify(idObjJson)


# Get recipe info for a given name
@app.route("/recipes/name/<string:name>")
def recipeByName(name):
    parsedName = name.replace("+", " ")
    nameQuery = db.session.query(Recipes).filter(Recipes.name == parsedName)
    nameQueryJson = recipesSchema.dump(nameQuery)

    if len(nameQueryJson) == 0:
        return jsonify({"status": "Could not find Recipes instance with given name"})
    else:
        return jsonify(nameQueryJson)


"""
Inspired by: https://gitlab.com/daniamir/sweproject/-/blob/anthony-backend/backend/db.py
"""


def getMarineLifeInstances():
    mlJsonFile = open("./data/marine_life.json")
    mlJson = json.load(mlJsonFile)
    mlInstances = []
    # For data cleaning: ensure common names are unique
    mlDict = defaultdict(lambda: "null")

    for ml in mlJson:
        mlDict[ml["Species Name"]] = MarineLife(
            image=ml["image"],
            populationStatus=ml["population"],
            calories=str(ml["nutrition"]) + " calories",
            caloriesVal=ml["nutrition"],
            availability=ml["availability"],
            idFeatures=ml["identifying features"],
            commonName=ml["Species Name"],
            scientificName=ml["Scientific Name"],
            regions=ml["Location"],
            waterBody=ml["Location"],
            harvestType=ml["harvest type"],
            quote=ml["quote"],
            locationMap=ml["Location"],
            ingredients=str(ml["ingredients"]),
            recipes=str(ml["recipes"]),
        )

    for k in mlDict:
        mlInstances.append(mlDict[k])

    return mlInstances


"""
Inspired by: https://gitlab.com/daniamir/sweproject/-/blob/anthony-backend/backend/db.py
"""


def getRecipesInstances():
    recpJsonFile = open("./data/recipes.json")
    recpJson = json.load(recpJsonFile)
    recpInstances = []
    # For data cleaning: ensure recipe titles are unique
    recpDict = dict()

    for recp in recpJson:
        if recp["title"] in recpDict:
            if recp["fish"] not in recpDict[recp["title"]]["fishType"]:
                # Aggregate all the fish types for a given recipe into one entry
                recpDict[recp["title"]]["fishType"].append(recp["fish"])
        else:
            recpDict[recp["title"]] = {
                "name": recp["title"],
                "image": recp["image"],
                "instructions": recp["instrcutions"],
                "numServings": recp["servings"],
                "prepTime": recp["readyInMinutes"],
                "rating": recp["rating"],
                "cost": recp["pricePerServing"],
                "ingredients": recp["ingredients"],
                "fishType": [recp["fish"]],
                "diet": recp["diet"],
            }

    for k in recpDict:
        recpDict[k] = Recipes(
            name=recpDict[k]["name"],
            image=recpDict[k]["image"],
            instructions=recpDict[k]["instructions"],
            numServings=str(recpDict[k]["numServings"]) + " servings",
            numServingsVal=recpDict[k]["numServings"],
            prepTime=str(recpDict[k]["prepTime"]) + " minutes",
            prepTimeVal=recpDict[k]["prepTime"],
            rating=str(int(recpDict[k]["rating"])) + "/100",
            ratingVal=recpDict[k]["rating"],
            cost="$" + format(round(recpDict[k]["cost"] / 100, 2), ".2f"),
            costVal=recpDict[k]["cost"],
            ingredients=json.dumps(list(recpDict[k]["ingredients"])),
            fishType=json.dumps(list(recpDict[k]["fishType"])),
            diet=json.dumps(recpDict[k]["diet"]),
            media=recpDict[k]["media"],
        )
        recpInstances.append(recpDict[k])

    return recpInstances


def getIngredientsInstances():
    ingJsonFile = open("./data/ingredients.json")
    ingJson = json.load(ingJsonFile)
    ingInstances = []
    # For data cleaning: ensure ingredient names are unique
    ingDict = defaultdict(lambda: "null")

    for ing in ingJson:
        ingDict[ing["name"]] = Ingredients(
            ingName=ing["name"],
            image=ing["image"],
            price="$" + format(round(ing["price"] / 100, 2), ".2f"),
            priceVal=ing["price"],
            aisle=ing["aisle"],
            consistency=ing["consistency"],
            fishType=json.dumps(ing["species"]),
            recipes=json.dumps(ing["recipes"]),
            nutrition=json.dumps(
                ing["nutrition"]
            ),  # needs json.load, but three sub properties?
            percentProtein=str(ing["nutrition"]["caloricBreakdown"]["percentProtein"])
            + "%",
            percentFat=str(ing["nutrition"]["caloricBreakdown"]["percentFat"]) + "%",
            percentFatVal=ing["nutrition"]["caloricBreakdown"]["percentFat"],
            percentCarbs=str(ing["nutrition"]["caloricBreakdown"]["percentCarbs"])
            + "%",
            weightPerServing=str(ing["nutrition"]["weightPerServing"]["amount"])
            + " "
            + str(ing["nutrition"]["weightPerServing"]["unit"]),
            media=ing["media"],
        )
    for k in ingDict:
        ingInstances.append(ingDict[k])

    return ingInstances


def populateDB():
    # Create tables
    db.create_all()

    # COMMENT THESE IN ONLY IF YOU WANT TO POPULATE A PARTICULAR TABLE!

    # # Grab the data
    # marineLifeInstances = getMarineLifeInstances()
    # recipesInstances = getRecipesInstances()
    # ingredientsInstances = getIngredientsInstances()

    # # Stage the changes
    # db.session.add_all(marineLifeInstances)
    # db.session.add_all(recipesInstances)
    # db.session.add_all(ingredientsInstances)

    ## This is necessary to save the changes on the db connection
    db.session.commit()


if __name__ == "__main__":
    # populateDB()
    app.run(host="0.0.0.0", port=5000, debug=True)
