# Seastainable



## Canvas / Discord group number: 
10AM Group 6

## Names of the team members (NAME, GITLABID, UTEID):
* Caleb Park, progammer, cap4987
* Carlos Vela, carlosvela4567, cjv828
* Geethika Hemukmar, geethikah21, gh22885 
* Mara Manskie, maramanskie, mem7249
* Derrick Liu, dliu0322, drl2395

## Name of Project: Seastainable

## Project Proposal
This website aims to inform the user about sustainable seafood in order to stop overfishing in certain at-risk fish populations.

## Git SHA: 3b9db0eef9791bf687d02453a7c48b8331fc7971

## Phase 1 Leader: Mara Manskie
## Phase 2 Leader: Caleb Park
## Phase 3 Leader: Geethika Hemukmar
## Phase 4 Leader: Carlos Vela

## Gitlab URL: https://gitlab.com/maramanskie/seastainable

## Gitlab Pipeline: https://gitlab.com/maramanskie/seastainable/-/pipelines

## Postman Documentation: https://documenter.getpostman.com/view/20307881/Uyr4Kfk4

## Website: https://seastainable.me

## Time Table Phase 1
Derrick: 
* Estimated time - 10 hours 
* Actual time - 22 hours

Mara
* Estimated time - 15 hours 
* Actual time - 32 hours

Caleb
* Estimated time - 12 hours 
* Actual time - 22 hours

Carlos
* Estimated time - 25 hours 
* Actual time - 22 hours

Geethika
* Estimated time - 30 hours 
* Actual time - 22 hours


## Time Table Phase 2:
Derrick: 
* Estimated time - 33 hours 
* Actual time - 38 hours

Mara
* Estimated time - 35 hours 
* Actual time - 42 hours

Caleb
* Estimated time - 38 hours 
* Actual time - 50 hours

Carlos
* Estimated time - 32 hours 
* Actual time - 37 hours

Geethika
* Estimated time - 35 hours 
* Actual time - 48 hours

## Time Table Phase 3
Derrick: 
* Estimated time - 10 hours 
* Actual time - 15 hours

Mara
* Estimated time - 15 hours 
* Actual time - 17 hours

Caleb
* Estimated time - 15 hours 
* Actual time - 20 hours

Carlos
* Estimated time - 10 hours 
* Actual time - 16 hours

Geethika
* Estimated time - 15 hours 
* Actual time - 21 hours

## Time Table Phase 4
Derrick: 
* Estimated time - 5 hours 
* Actual time - 7 hours

Mara
* Estimated time - 8 hours 
* Actual time - 12 hours

Caleb
* Estimated time - 8 hours 
* Actual time - 15 hours

Carlos
* Estimated time - 7 hours 
* Actual time - 8 hours

Geethika
* Estimated time - 9 hours 
* Actual time - 12 hours




